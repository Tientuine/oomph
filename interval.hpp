/* oomph.hpp -- Include file for oomph

Copyright (c) 2011, 2012, Matthew Adam Johnson.

This file is part of the OOMPH Library.

The OOMPH Library is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version.

The OOMPH Library is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the OOMPH Library; see the file COPYING.LIB.  If not, write to
the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
MA 02110-1301, USA. */

#ifndef HPP_INTERVAL
#define HPP_INTERVAL

#include <cstring>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <utility>
#include <vector>
#include "gmp.h"
#include "mpfi.h"
#include "mpfi_io.h"

/**************** Function objects ****************/
/* Any evaluation of a __oomph_expr ends up calling one of these functions
   all intermediate functions being inline, the evaluation should optimize
   to a direct call to the relevant function, thus yielding no overhead
   over the C interface. */

struct __oomph_unary_plus
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_set(a, b); }
};

struct __oomph_unary_minus
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_neg(a, b); }
};

struct __oomph_binary_plus
{
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfi_srcptr c)
  { mpfi_add(a, b, c); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, unsigned long int l)
  { mpfi_add_ui(a, b, l); }
  static void eval(mpfi_ptr a, unsigned long int l, mpfi_srcptr b)
  { mpfi_add_ui(a, b, l); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, signed long int l)
  { mpfi_add_si(a, b, l); }
  static void eval(mpfi_ptr a, signed long int l, mpfi_srcptr b)
  { mpfi_add_si(a, b, l); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, double d)
  { mpfi_add_d(a, b, d); }
  static void eval(mpfi_ptr a, double d, mpfi_srcptr b)
  { mpfi_add_d(a, b, d); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpz_srcptr z)
  { mpfi_add_z(a, b, z); }
  static void eval(mpfi_ptr a, mpz_srcptr z, mpfi_srcptr b)
  { mpfi_add_z(a, b, z); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpq_srcptr q)
  { mpfi_add_q(a, b, q); }
  static void eval(mpfi_ptr a, mpq_srcptr q, mpfi_srcptr b)
  { mpfi_add_q(a, b, q); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfr_srcptr f)
  { mpfi_add_fr(a, b, f); }
  static void eval(mpfi_ptr a, mpfr_srcptr f, mpfi_srcptr b)
  { mpfi_add_fr(a, b, f); }
};

struct __oomph_binary_minus
{
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfi_srcptr c)
  { mpfi_sub(a, b, c); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, unsigned long int l)
  { mpfi_sub_ui(a, b, l); }
  static void eval(mpfi_ptr a, unsigned long int l, mpfi_srcptr b)
  { mpfi_ui_sub(a, l, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, signed long int l)
  { mpfi_sub_si(a, b, l); }
  static void eval(mpfi_ptr a, signed long int l, mpfi_srcptr b)
  { mpfi_si_sub(a, l, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, double d)
  { mpfi_sub_d(a, b, d); }
  static void eval(mpfi_ptr a, double d, mpfi_srcptr b)
  { mpfi_d_sub(a, d, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpz_srcptr z)
  { mpfi_sub_z(a, b, z); }
  static void eval(mpfi_ptr a, mpz_srcptr z, mpfi_srcptr b)
  { mpfi_z_sub(a, z, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpq_srcptr q)
  { mpfi_sub_q(a, b, q); }
  static void eval(mpfi_ptr a, mpq_srcptr q, mpfi_srcptr b)
  { mpfi_q_sub(a, q, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfr_srcptr f)
  { mpfi_sub_fr(a, b, f); }
  static void eval(mpfi_ptr a, mpfr_srcptr f, mpfi_srcptr b)
  { mpfi_fr_sub(a, f, b); }
};

struct __oomph_binary_multiplies
{
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfi_srcptr c)
  { mpfi_mul(a, b, c); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, unsigned long int l)
  { mpfi_mul_ui(a, b, l); }
  static void eval(mpfi_ptr a, unsigned long int l, mpfi_srcptr b)
  { mpfi_mul_ui(a, b, l); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, signed long int l)
  { mpfi_mul_si(a, b, l); }
  static void eval(mpfi_ptr a, signed long int l, mpfi_srcptr b)
  { mpfi_mul_si(a, b, l); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, double d)
  { mpfi_mul_d(a, b, d); }
  static void eval(mpfi_ptr a, double d, mpfi_srcptr b)
  { mpfi_mul_d(a, b, d); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpz_srcptr z)
  { mpfi_mul_z(a, b, z); }
  static void eval(mpfi_ptr a, mpz_srcptr z, mpfi_srcptr b)
  { mpfi_mul_z(a, b, z); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpq_srcptr q)
  { mpfi_mul_q(a, b, q); }
  static void eval(mpfi_ptr a, mpq_srcptr q, mpfi_srcptr b)
  { mpfi_mul_q(a, b, q); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfr_srcptr f)
  { mpfi_mul_fr(a, b, f); }
  static void eval(mpfi_ptr a, mpfr_srcptr f, mpfi_srcptr b)
  { mpfi_mul_fr(a, b, f); }
};

struct __oomph_binary_divides
{
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfi_srcptr c)
  { mpfi_div(a, b, c); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, unsigned long int l)
  { mpfi_div_ui(a, b, l); }
  static void eval(mpfi_ptr a, unsigned long int l, mpfi_srcptr b)
  { mpfi_ui_div(a, l, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, signed long int l)
  { mpfi_div_si(a, b, l); }
  static void eval(mpfi_ptr a, signed long int l, mpfi_srcptr b)
  { mpfi_si_div(a, l, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, double d)
  { mpfi_div_d(a, b, d); }
  static void eval(mpfi_ptr a, double d, mpfi_srcptr b)
  { mpfi_d_div(a, d, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpz_srcptr z)
  { mpfi_div_z(a, b, z); }
  static void eval(mpfi_ptr a, mpz_srcptr z, mpfi_srcptr b)
  { mpfi_z_div(a, z, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpq_srcptr q)
  { mpfi_div_q(a, b, q); }
  static void eval(mpfi_ptr a, mpq_srcptr q, mpfi_srcptr b)
  { mpfi_q_div(a, q, b); }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfr_srcptr f)
  { mpfi_div_fr(a, b, f); }
  static void eval(mpfi_ptr a, mpfr_srcptr f, mpfi_srcptr b)
  { mpfi_fr_div(a, f, b); }
};

struct __oomph_binary_equal
{
  static bool eval(mpfi_srcptr a, mpfi_srcptr b)
  { return mpfi_cmp(a, b) == 0; }
  static bool eval(mpfi_srcptr a, unsigned long int l)
  { return mpfi_cmp_ui(a, l) == 0; }
  static bool eval(unsigned long int l, mpfi_srcptr a)
  { return mpfi_cmp_ui(a, l) == 0; }
  static bool eval(mpfi_srcptr a, signed long int l)
  { return mpfi_cmp_si(a, l) == 0; }
  static bool eval(signed long int l, mpfi_srcptr a)
  { return mpfi_cmp_si(a, l) == 0; }
  static bool eval(mpfi_srcptr a, double d)
  { return mpfi_cmp_d(a, d) == 0; }
  static bool eval(double d, mpfi_srcptr a)
  { return mpfi_cmp_d(a, d) == 0; }
  static bool eval(mpfi_srcptr a, mpz_srcptr z)
  { return mpfi_cmp_z(a, z) == 0; }
  static bool eval(mpz_srcptr z, mpfi_srcptr a)
  { return mpfi_cmp_z(a, z) == 0; }
  static bool eval(mpfi_srcptr a, mpq_srcptr q)
  { return mpfi_cmp_q(a, q) == 0; }
  static bool eval(mpq_srcptr q, mpfi_srcptr a)
  { return mpfi_cmp_q(a, q) == 0; }
  static bool eval(mpfi_srcptr a, mpfr_srcptr f)
  { return mpfi_cmp_fr(a, f) == 0; }
  static bool eval(mpfr_srcptr f, mpfi_srcptr a)
  { return mpfi_cmp_fr(a, f) == 0; }
};

struct __oomph_binary_not_equal
{
  template <typename T, typename U>
  static bool eval(T t, U u) { return __oomph_binary_equal::eval(t, u) == false; }
};

struct __oomph_binary_less
{
  static bool eval(mpfi_srcptr a, mpfi_srcptr b)
  { return mpfi_cmp(a, b) < 0; }
  static bool eval(mpfi_srcptr a, unsigned long int l)
  { return mpfi_cmp_ui(a, l) < 0; }
  static bool eval(unsigned long int l, mpfi_srcptr a)
  { return mpfi_cmp_ui(a, l) > 0; }
  static bool eval(mpfi_srcptr a, signed long int l)
  { return mpfi_cmp_si(a, l) < 0; }
  static bool eval(signed long int l, mpfi_srcptr a)
  { return mpfi_cmp_si(a, l) > 0; }
  static bool eval(mpfi_srcptr a, double d)
  { return mpfi_cmp_d(a, d) < 0; }
  static bool eval(double d, mpfi_srcptr a)
  { return mpfi_cmp_d(a, d) > 0; }
  static bool eval(mpfi_srcptr a, mpz_srcptr z)
  { return mpfi_cmp_z(a, z) < 0; }
  static bool eval(mpz_srcptr z, mpfi_srcptr a)
  { return mpfi_cmp_z(a, z) > 0; }
  static bool eval(mpfi_srcptr a, mpq_srcptr q)
  { return mpfi_cmp_q(a, q) < 0; }
  static bool eval(mpq_srcptr q, mpfi_srcptr a)
  { return mpfi_cmp_q(a, q) > 0; }
  static bool eval(mpfi_srcptr a, mpfr_srcptr f)
  { return mpfi_cmp_fr(a, f) < 0; }
  static bool eval(mpfr_srcptr f, mpfi_srcptr a)
  { return mpfi_cmp_fr(a, f) > 0; }
};

struct __oomph_binary_greater
{
  static bool eval(mpfi_srcptr a, mpfi_srcptr g)
  { return mpfi_cmp(a, g) > 0; }
  static bool eval(mpfi_srcptr a, unsigned long int l)
  { return mpfi_cmp_ui(a, l) > 0; }
  static bool eval(unsigned long int l, mpfi_srcptr a)
  { return mpfi_cmp_ui(a, l) < 0; }
  static bool eval(mpfi_srcptr a, signed long int l)
  { return mpfi_cmp_si(a, l) > 0; }
  static bool eval(signed long int l, mpfi_srcptr a)
  { return mpfi_cmp_si(a, l) < 0; }
  static bool eval(mpfi_srcptr a, double d)
  { return mpfi_cmp_d(a, d) > 0; }
  static bool eval(double d, mpfi_srcptr a)
  { return mpfi_cmp_d(a, d) < 0; }
  static bool eval(mpfi_srcptr a, mpz_srcptr z)
  { return mpfi_cmp_z(a, z) > 0; }
  static bool eval(mpz_srcptr z, mpfi_srcptr a)
  { return mpfi_cmp_z(a, z) < 0; }
  static bool eval(mpfi_srcptr a, mpq_srcptr q)
  { return mpfi_cmp_q(a, q) > 0; }
  static bool eval(mpq_srcptr q, mpfi_srcptr a)
  { return mpfi_cmp_q(a, q) < 0; }
  static bool eval(mpfi_srcptr a, mpfr_srcptr f)
  { return mpfi_cmp_fr(a, f) > 0; }
  static bool eval(mpfr_srcptr f, mpfi_srcptr a)
  { return mpfi_cmp_fr(a, f) < 0; }
};

struct __oomph_binary_less_equal
{
  template <typename T, typename U>
  static bool eval(T t, U u) { return __oomph_binary_greater::eval(t, u) == false; }
};

struct __oomph_binary_greater_equal
{
  template <typename T, typename U>
  static bool eval(T t, U u) { return __oomph_binary_less::eval(t, u) == false; }
};

struct __oomph_unary_increment
{
  static void eval(mpfi_ptr a) { mpfi_add_ui(a, a, 1); }
};

struct __oomph_unary_decrement
{
  static void eval(mpfi_ptr a) { mpfi_sub_ui(a, a, 1); }
};

struct __oomph_abs_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_abs(a, b); }
};

struct __oomph_sqrt_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_sqrt(a, b); }
};

/* BEG NEW STUFF - Like TRIG, etc. */

struct __oomph_cbrt_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_cbrt(a, b); }
};

struct __oomph_sqr_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_sqr(a, b); }
};

struct __oomph_inv_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_inv(a, b); }
};

struct __oomph_log_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_log(a, b); }
};

struct __oomph_log1p_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_log1p(a, b); }
};

struct __oomph_log2_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_log2(a, b); }
};

struct __oomph_log10_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_log10(a, b); }
};

struct __oomph_exp_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_exp(a, b); }
};

struct __oomph_expm1_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_expm1(a, b); }
};

struct __oomph_exp2_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_exp2(a, b); }
};

struct __oomph_cos_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_cos(a, b); }
};

struct __oomph_sin_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_sin(a, b); }
};

struct __oomph_tan_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_tan(a, b); }
};

struct __oomph_acos_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_acos(a, b); }
};

struct __oomph_asin_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_asin(a, b); }
};

struct __oomph_atan_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_atan(a, b); }
};

struct __oomph_atan2_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfi_srcptr c) { mpfi_atan2(a, b, c); }
};

struct __oomph_sec_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_sec(a, b); }
};

struct __oomph_csc_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_csc(a, b); }
};

struct __oomph_cot_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_cot(a, b); }
};

struct __oomph_cosh_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_cosh(a, b); }
};

struct __oomph_sinh_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_sinh(a, b); }
};

struct __oomph_tanh_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_tanh(a, b); }
};

struct __oomph_acosh_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_acosh(a, b); }
};

struct __oomph_asinh_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_asinh(a, b); }
};

struct __oomph_atanh_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_atanh(a, b); }
};

struct __oomph_sech_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_sech(a, b); }
};

struct __oomph_csch_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_csch(a, b); }
};

struct __oomph_coth_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b) { mpfi_coth(a, b); }
};

/* END NEW STUFF */

struct __oomph_hypot_function
{
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfi_srcptr c)
  { mpfi_hypot(a, b, c);  }
  static void eval(mpfi_ptr a, mpfi_srcptr b, unsigned long int l)
  {
    mpfi_t temp;
    mpfi_init_set_ui(temp, l);
    mpfi_hypot(a, b, temp);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, unsigned long int l, mpfi_srcptr b)
  {
    mpfi_t temp;
    mpfi_init_set_ui(temp, l);
    mpfi_hypot(a, temp, b);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, mpfi_srcptr b, signed long int l)
  {
    mpfi_t temp;
    mpfi_init_set_si(temp, l);
    mpfi_hypot(a, b, temp);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, signed long int l, mpfi_srcptr b)
  {
    mpfi_t temp;
    mpfi_init_set_si(temp, l);
    mpfi_hypot(a, temp, b);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, mpfi_srcptr b, double d)
  {
    mpfi_t temp;
    mpfi_init_set_d(temp, d);
    mpfi_hypot(a, b, temp);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, double d, mpfi_srcptr b)
  {
    mpfi_t temp;
    mpfi_init_set_d(temp, d);
    mpfi_hypot(a, temp, b);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpz_srcptr z)
  {
    mpfi_t temp;
    mpfi_init_set_z(temp, z);
    mpfi_hypot(a, b, temp);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, mpz_srcptr z, mpfi_srcptr b)
  {
    mpfi_t temp;
    mpfi_init_set_z(temp, z);
    mpfi_hypot(a, temp, b);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpq_srcptr q)
  {
    mpfi_t temp;
    mpfi_init_set_q(temp, q);
    mpfi_hypot(a, b, temp);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, mpq_srcptr q, mpfi_srcptr b)
  {
    mpfi_t temp;
    mpfi_init_set_q(temp, q);
    mpfi_hypot(a, temp, b);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, mpfi_srcptr b, mpfr_srcptr h)
  {
    mpfi_t temp;
    mpfi_init_set_fr(temp, h);
    mpfi_hypot(a, b, temp);
    mpfi_clear(temp);
  }
  static void eval(mpfi_ptr a, mpfr_srcptr h, mpfi_srcptr b)
  {
    mpfi_t temp;
    mpfi_init_set_fr(temp, h);
    mpfi_hypot(a, temp, b);
    mpfi_clear(temp);
  }

};

struct __oomph_sgn_function
{
  static int eval(mpfi_srcptr a) { return mpfi_cmp_ui(a, 0); }
};

struct __oomph_cmp_function
{
  static int eval(mpfi_srcptr a, mpfi_srcptr b)
  { return mpfi_cmp(a, b); }
  static int eval(mpfi_srcptr a, unsigned long int l)
  { return mpfi_cmp_ui(a, l); }
  static int eval(unsigned long int l, mpfi_srcptr a)
  { return -mpfi_cmp_ui(a, l); }
  static int eval(mpfi_srcptr a, signed long int l)
  { return mpfi_cmp_si(a, l); }
  static int eval(signed long int l, mpfi_srcptr a)
  { return -mpfi_cmp_si(a, l); }
  static int eval(mpfi_srcptr a, double d)
  { return mpfi_cmp_d(a, d); }
  static int eval(double d, mpfi_srcptr a)
  { return -mpfi_cmp_d(a, d); }
  static int eval(mpfi_srcptr a, mpz_srcptr z)
  { return mpfi_cmp_z(a, z); }
  static int eval(mpz_srcptr z, mpfi_srcptr a)
  { return -mpfi_cmp_z(a, z); }
  static int eval(mpfi_srcptr a, mpq_srcptr q)
  { return mpfi_cmp_q(a, q); }
  static int eval(mpq_srcptr q, mpfi_srcptr a)
  { return -mpfi_cmp_q(a, q); }
  static int eval(mpfi_srcptr a, mpfr_srcptr g)
  { return mpfi_cmp_fr(a, g); }
  static int eval(mpfr_srcptr g, mpfi_srcptr a)
  { return -mpfi_cmp_fr(a, g); }
};

struct __oomph_rand_function
{
  static void eval(mpfr_ptr f, mpfi_srcptr a, gmp_randstate_t s)
  { mpfi_urandom(f, a, s); }
};

/**************** Auxiliary classes ****************/

// general expression template class
template <class T, class U>
class __oomph_expr;

// templates for resolving expression types
template <class T>
struct __oomph_resolve_ref
{
  typedef T ref_type;
};

template <class T, class U>
struct __oomph_resolve_ref<__oomph_expr<T, U> >
{
  typedef const __oomph_expr<T, U> & ref_type;
};


template <class T, class U = T>
struct __oomph_resolve_expr;

template <>
struct __oomph_resolve_expr<mpfr_t>
{
  typedef mpfr_t value_type;
  typedef mpfr_ptr ptr_type;
};

template <>
struct __oomph_resolve_expr<mpfi_t>
{
  typedef mpfi_t value_type;
  typedef mpfi_ptr ptr_type;
};

template <>
struct __oomph_resolve_expr<mpfr_t, mpfi_t>
{
  typedef mpfi_t value_type;
};

template <>
struct __oomph_resolve_expr<mpfi_t, mpfr_t>
{
  typedef mpfi_t value_type;
};

template <class T, class U, class V>
struct __oomph_resolve_temp
{
  typedef __oomph_expr<T, T> temp_type;
};

template <class T>
struct __oomph_resolve_temp<T, T, T>
{
  typedef const __oomph_expr<T, T> & temp_type;
};

// classes for evaluating unary and binary expressions
template <class T, class Op>
struct __oomph_unary_expr
{
  const T &val;

  __oomph_unary_expr(const T &v) : val(v) { }
private:
  __oomph_unary_expr();
};

template <class T, class U, class Op>
struct __oomph_binary_expr
{
  typename __oomph_resolve_ref<T>::ref_type val1;
  typename __oomph_resolve_ref<U>::ref_type val2;

  __oomph_binary_expr(const T &v1, const U &v2) : val1(v1), val2(v2) { }
private:
  __oomph_binary_expr();
};


// functions for evaluating expressions
template <class T, class U>
void __oomph_set_expr(mpfi_ptr, const __oomph_expr<T, U> &);


/**************** Macros for in-class declarations ****************/
/* This is just repetitive code that is easier to maintain if it's written
   only once */

#define __GMPP_DECLARE_COMPOUND_OPERATOR(fun)                         \
  template <class T, class U>                                         \
  __oomph_expr<value_type, value_type> & fun(const __oomph_expr<T, U> &);

#define __GMPN_DECLARE_COMPOUND_OPERATOR(fun) \
  __oomph_expr & fun(signed char);              \
  __oomph_expr & fun(unsigned char);            \
  __oomph_expr & fun(signed int);               \
  __oomph_expr & fun(unsigned int);             \
  __oomph_expr & fun(signed short int);         \
  __oomph_expr & fun(unsigned short int);       \
  __oomph_expr & fun(signed long int);          \
  __oomph_expr & fun(unsigned long int);        \
  __oomph_expr & fun(float);                    \
  __oomph_expr & fun(double);                   \
  __oomph_expr & fun(long double);

#define __GMP_DECLARE_COMPOUND_OPERATOR(fun) \
__GMPP_DECLARE_COMPOUND_OPERATOR(fun)        \
__GMPN_DECLARE_COMPOUND_OPERATOR(fun)

#define __GMP_DECLARE_COMPOUND_OPERATOR_UI(fun) \
  __oomph_expr & fun(unsigned long int);

#define __GMP_DECLARE_INCREMENT_OPERATOR(fun) \
  inline __oomph_expr & fun();                  \
  inline __oomph_expr fun(int);

/**************** mpfi_class -- wrapper for mpfi_t ****************/

template <>
class __oomph_expr<mpfi_t, mpfi_t>
{
private:
  typedef mpfi_t value_type;
  value_type mp;
public:
  mpfr_prec_t get_prec() const { return mpfi_get_prec(mp); }

  void set_prec(mpfr_prec_t prec) { mpfi_set_prec(mp, prec); }
  //void set_prec_raw(mp_bitcnt_t prec) { mpfi_set_prec_raw(mp, prec); }

  // constructors and destructor
  __oomph_expr() { mpfi_init(mp); }

  __oomph_expr(const __oomph_expr &f)
  { mpfi_init2(mp, f.get_prec()); mpfi_set(mp, f.mp); }
  __oomph_expr(const __oomph_expr &f, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set(mp, f.mp); }
  template <class T, class U>
  __oomph_expr(const __oomph_expr<T, U> &expr)
  { mpfi_init2(mp, expr.get_prec()); __oomph_set_expr(mp, expr); }
  template <class T, class U>
  __oomph_expr(const __oomph_expr<T, U> &expr, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); __oomph_set_expr(mp, expr); }

  __oomph_expr(signed char c) { mpfi_init_set_si(mp, c); }
  __oomph_expr(signed char c, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_si(mp, c); }
  __oomph_expr(unsigned char c) { mpfi_init_set_ui(mp, c); }
  __oomph_expr(unsigned char c, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_ui(mp, c); }

  __oomph_expr(signed int i) { mpfi_init_set_si(mp, i); }
  __oomph_expr(signed int i, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_si(mp, i); }
  __oomph_expr(unsigned int i) { mpfi_init_set_ui(mp, i); }
  __oomph_expr(unsigned int i, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_ui(mp, i); }

  __oomph_expr(signed short int s) { mpfi_init_set_si(mp, s); }
  __oomph_expr(signed short int s, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_si(mp, s); }
  __oomph_expr(unsigned short int s) { mpfi_init_set_ui(mp, s); }
  __oomph_expr(unsigned short int s, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_ui(mp, s); }

  __oomph_expr(signed long int l) { mpfi_init_set_si(mp, l); }
  __oomph_expr(signed long int l, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_si(mp, l); }
  __oomph_expr(unsigned long int l) { mpfi_init_set_ui(mp, l); }
  __oomph_expr(unsigned long int l, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_ui(mp, l); }

  __oomph_expr(float f) { mpfi_init_set_d(mp, f); }
  __oomph_expr(float f, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_d(mp, f); }
  __oomph_expr(double d) { mpfi_init_set_d(mp, d); }
  __oomph_expr(double d, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set_d(mp, d); }
  // __oomph_expr(long double ld) { mpf_init_set_d(mp, ld); }
  // __oomph_expr(long double ld, mp_bitcnt_t prec)
  // { mpf_init2(mp, prec); mpf_set_d(mp, ld); }

  explicit __oomph_expr(const char *s)
  {
    if (mpfi_init_set_str (mp, s, 0) != 0)
      {
        mpfi_clear (mp);
        throw std::invalid_argument ("mpfi_set_str");
      }
  }
  __oomph_expr(const char *s, mp_bitcnt_t prec, int base = 10)
  {
    mpfi_init2(mp, prec);
    if (mpfi_set_str(mp, s, base) != 0)
      {
        mpfi_clear (mp);
        throw std::invalid_argument ("mpfi_set_str");
      }
  }
  explicit __oomph_expr(const std::string &s)
  {
    if (mpfi_init_set_str(mp, s.c_str(), 0) != 0)
      {
        mpfi_clear (mp);
        throw std::invalid_argument ("mpfi_set_str");
      }
  }
  __oomph_expr(const std::string &s, mp_bitcnt_t prec, int base = 10)
  {
    mpfi_init2(mp, prec);
    if (mpfi_set_str(mp, s.c_str(), base) != 0)
      {
        mpfi_clear (mp);
        throw std::invalid_argument ("mpfi_set_str");
      }
  }

  explicit __oomph_expr(mpfi_srcptr f)
  { mpfi_init2(mp, mpfi_get_prec(f)); mpfi_set(mp, f); }
  __oomph_expr(mpfi_srcptr f, mp_bitcnt_t prec)
  { mpfi_init2(mp, prec); mpfi_set(mp, f); }

  ~__oomph_expr() { mpfi_clear(mp); }

  // assignment operators
  __oomph_expr & operator=(const __oomph_expr &f)
  { mpfi_set(mp, f.mp); return *this; }
  template <class T, class U>
  __oomph_expr<value_type, value_type> & operator=(const __oomph_expr<T, U> &expr)
  { __oomph_set_expr(mp, expr); return *this; }

  __oomph_expr & operator=(signed char c) { mpfi_set_si(mp, c); return *this; }
  __oomph_expr & operator=(unsigned char c) { mpfi_set_ui(mp, c); return *this; }

  __oomph_expr & operator=(signed int i) { mpfi_set_si(mp, i); return *this; }
  __oomph_expr & operator=(unsigned int i) { mpfi_set_ui(mp, i); return *this; }

  __oomph_expr & operator=(signed short int s)
  { mpfi_set_si(mp, s); return *this; }
  __oomph_expr & operator=(unsigned short int s)
  { mpfi_set_ui(mp, s); return *this; }

  __oomph_expr & operator=(signed long int l)
  { mpfi_set_si(mp, l); return *this; }
  __oomph_expr & operator=(unsigned long int l)
  { mpfi_set_ui(mp, l); return *this; }

  __oomph_expr & operator=(float f) { mpfi_set_d(mp, f); return *this; }
  __oomph_expr & operator=(double d) { mpfi_set_d(mp, d); return *this; }
  // __oomph_expr & operator=(long double ld)
  // { mpfi_set_ld(mp, ld); return *this; }

  __oomph_expr & operator=(const char *s)
  {
    if (mpfi_set_str (mp, s, 0) != 0)
      throw std::invalid_argument ("mpfi_set_str");
    return *this;
  }
  __oomph_expr & operator=(const std::string &s)
  {
    if (mpfi_set_str(mp, s.c_str(), 0) != 0)
      throw std::invalid_argument ("mpfi_set_str");
    return *this;
  }

  // string input/output functions
  int set_str(const char *s, int base = 10)
  { return mpfi_set_str(mp, s, base); }
  int set_str(const std::string &s, int base = 10)
  { return mpfi_set_str(mp, s.c_str(), base); }
  std::string get_str(int base = 10) const
  {
    char* sinf {nullptr};
    char* ssup {nullptr};

    mp_exp_t einf, esup;

    std::ostringstream os;

    mpfr_prec_t P {get_prec()};

    mpfr_ptr inf {new mpfr_t};
    mpfr_init2(inf, P);
    mpfi_get_left(inf, mp);

    mpfr_ptr sup {new mpfr_t};
    mpfr_init2(sup, P);
    mpfi_get_right(sup, mp);

    P = std::min(os.precision(),P);

    sinf = mpfr_get_str (sinf, &einf, base, P, inf, GMP_RNDD );
    ssup = mpfr_get_str (ssup, &esup, base, P, sup, GMP_RNDU );

    char const* sinfp {sinf};
    char const* ssupp {ssup};

    os << '[';

    if (*sinf == '-') { os << *sinfp++; }

    os << *sinfp++;
    os << '.' << sinfp << 'e' << (einf-1) << ',';

    if (*ssup == '-') { os << *ssupp++; }

    os << *ssupp++;
    os << '.' << ssupp << 'e' << (esup-1) << ']';

    mpfr_clear(inf);
    mpfr_clear(sup);
    delete inf;
    delete sup;

    mpfr_free_str(sinf);
    mpfr_free_str(ssup);

    return os.str();
  }

  // conversion functions
  mpfi_srcptr __get_mp() const { return mp; }
  mpfi_ptr __get_mp() { return mp; }
  mpfi_srcptr get_mpfi_t() const { return mp; }
  mpfi_ptr get_mpfi_t() { return mp; }

  //signed long int get_si() const { return mpfi_get_si(mp); }
  //unsigned long int get_ui() const { return mpfi_get_ui(mp); }
  double get_d() const { return mpfi_get_d(mp); }

  // bool fits_schar_p() const { return mpf_fits_schar_p(mp); }
  // bool fits_uchar_p() const { return mpf_fits_uchar_p(mp); }
  //bool fits_sint_p() const { return mpfi_fits_sint_p(mp); }
  //bool fits_uint_p() const { return mpfi_fits_uint_p(mp); }
  //bool fits_sshort_p() const { return mpfi_fits_sshort_p(mp); }
  //bool fits_ushort_p() const { return mpfi_fits_ushort_p(mp); }
  //bool fits_slong_p() const { return mpfi_fits_slong_p(mp); }
  //bool fits_ulong_p() const { return mpfi_fits_ulong_p(mp); }
  // bool fits_float_p() const { return mpfi_fits_float_p(mp); }
  // bool fits_double_p() const { return mpfi_fits_double_p(mp); }
  // bool fits_ldouble_p() const { return mpfi_fits_ldouble_p(mp); }

  // compound assignments
  __GMP_DECLARE_COMPOUND_OPERATOR(operator+=)
  __GMP_DECLARE_COMPOUND_OPERATOR(operator-=)
  __GMP_DECLARE_COMPOUND_OPERATOR(operator*=)
  __GMP_DECLARE_COMPOUND_OPERATOR(operator/=)

  __GMP_DECLARE_COMPOUND_OPERATOR_UI(operator<<=)
  __GMP_DECLARE_COMPOUND_OPERATOR_UI(operator>>=)

  __GMP_DECLARE_INCREMENT_OPERATOR(operator++)
  __GMP_DECLARE_INCREMENT_OPERATOR(operator--)
};

typedef __oomph_expr<mpfi_t, mpfi_t> mpfi_class;


/**************** I/O operators ****************/

// these should (and will) be provided separately

template <class T>
inline std::ostream & operator<<(
std::ostream &o, const __oomph_expr<T, T> &expr)
{
  return o << expr.__get_mp();
}

template <class T, class U>
inline std::ostream & operator<<
(std::ostream &o, const __oomph_expr<T, U> &expr)
{
  __oomph_expr<T, T> temp(expr);
  return o << temp.__get_mp();
}

template <>
inline std::ostream & operator<<
(std::ostream &o, const mpfi_class &expr)
{
	char* sinf {nullptr};
	char* ssup {nullptr};

	mp_exp_t einf, esup;

	mpfr_prec_t P {expr.get_prec()};

	mpfr_ptr inf {new mpfr_t};
	mpfr_init2(inf, P);
	mpfi_get_left(inf, expr.__get_mp());

	mpfr_ptr sup {new mpfr_t};
	mpfr_init2(sup, P);
	mpfi_get_right(sup, expr.__get_mp());

	P = std::min(o.precision(),P);

	sinf = mpfr_get_str (sinf, &einf, 10, P, inf, GMP_RNDD );
	ssup = mpfr_get_str (ssup, &esup, 10, P, sup, GMP_RNDU );

	char const* sinfp {sinf};
	char const* ssupp {ssup};

	o << '[';

	if (*sinf == '-') { o << *sinfp++; }

	o << *sinfp++;
	o << '.' << sinfp << 'e' << (einf-1) << ',';

	if (*ssup == '-') { o << *ssupp++; }

	o << *ssupp++;
	o << '.' << ssupp << 'e' << (esup-1) << ']';

	mpfr_clear(inf);
	mpfr_clear(sup);
	delete inf;
	delete sup;

	mpfr_free_str(sinf);
	mpfr_free_str(ssup);

	return o;
}

template <class T>
inline std::istream & operator>>(std::istream &i, __oomph_expr<T, T> &expr)
{
  return i >> expr.__get_mp();
}

/*inline std::istream & operator>>(std::istream &i, mpq_class &q)
{
  i >> q.get_mpq_t();
  // q.canonicalize(); // you might want to uncomment this
  return i;
}*/

/**************** Functions for type conversion ****************/
 /*
template <>
inline void __oomph_set_expr(mpz_ptr z, const mpfi_class &w)
{
  mpfi_set_z(, w.get_mpz_t());
}

template <class T>
inline void __oomph_set_expr(mpz_ptr z, const __oomph_expr<mpz_t, T> &expr)
{
  expr.eval(z);
}

template <>
inline void __oomph_set_expr(mpz_ptr z, const mpq_class &q)
{
  mpz_set_q(z, q.get_mpq_t());
}

template <class T>
inline void __oomph_set_expr(mpz_ptr z, const __oomph_expr<mpq_t, T> &expr)
{
  mpq_class temp(expr);
  mpz_set_q(z, temp.get_mpq_t());
}

template <class T>
inline void __oomph_set_expr(mpz_ptr z, const mpf_class &f)
{
  mpz_set_f(z, f.get_mpf_t());
}

template <class T>
inline void __oomph_set_expr(mpz_ptr z, const __oomph_expr<mpf_t, T> &expr)
{
  mpf_class temp(expr);
  mpz_set_f(z, temp.get_mpf_t());
}

template <>
inline void __oomph_set_expr(mpq_ptr q, const mpz_class &z)
{
  mpq_set_z(q, z.get_mpz_t());
}

template <class T>
inline void __oomph_set_expr(mpq_ptr q, const __oomph_expr<mpz_t, T> &expr)
{
  mpz_class temp(expr);
  mpq_set_z(q, temp.get_mpz_t());
}

template <>
inline void __oomph_set_expr(mpq_ptr q, const mpq_class &r)
{
  mpq_set(q, r.get_mpq_t());
}

template <class T>
inline void __oomph_set_expr(mpq_ptr q, const __oomph_expr<mpq_t, T> &expr)
{
  expr.eval(q);
}

template <class T>
inline void __oomph_set_expr(mpq_ptr q, const mpf_class &f)
{
  mpq_set_f(q, f.get_mpf_t());
}

template <class T>
inline void __oomph_set_expr(mpq_ptr q, const __oomph_expr<mpf_t, T> &expr)
{
  mpf_class temp(expr);
  mpq_set_f(q, temp.get_mpf_t());
}

template <class T>
inline void __oomph_set_expr(mpf_ptr f, const mpz_class &z)
{
  mpf_set_z(f, z.get_mpz_t());
}

template <class T>
inline void __oomph_set_expr(mpf_ptr f, const __oomph_expr<mpz_t, T> &expr)
{
  mpz_class temp(expr);
  mpf_set_z(f, temp.get_mpz_t());
}

template <class T>
inline void __oomph_set_expr(mpf_ptr f, const mpq_class &q)
{
  mpf_set_q(f, q.get_mpq_t());
}

template <class T>
inline void __oomph_set_expr(mpf_ptr f, const __oomph_expr<mpq_t, T> &expr)
{
  mpq_class temp(expr);
  mpf_set_q(f, temp.get_mpq_t());
}

template <>
inline void __oomph_set_expr(mpf_ptr f, const mpf_class &g)
{
  mpf_set(f, g.get_mpf_t());
}

template <class T>
inline void __oomph_set_expr(mpf_ptr f, const __oomph_expr<mpf_t, T> &expr)
{
  expr.eval(f, mpf_get_prec(f));
}
*/

template <>
inline void __oomph_set_expr(mpfi_ptr f, const mpfi_class &g)
{
  mpfi_set(f, g.get_mpfi_t());
}

template <class T>
inline void __oomph_set_expr(mpfi_ptr f, const __oomph_expr<mpfi_t, T> &expr)
{
  expr.eval(f, mpfi_get_prec(f));
}


/**************** Specializations of __oomph_expr ****************/
/* The eval() method of __oomph_expr<T, U> evaluates the corresponding
   expression and assigns the result to its argument, which is either an
   mpz_t, mpq_t, or mpf_t as specified by the T argument.
   Compound expressions are evaluated recursively (temporaries are created
   to hold intermediate values), while for simple expressions the eval()
   method of the appropriate function object (available as the Op argument
   of either __oomph_unary_expr<T, Op> or __oomph_binary_expr<T, U, Op>) is
   called. */


/**************** Unary expressions ****************/
/* cases:
   - simple:   argument is mp*_class, that is, __oomph_expr<T, T>
   - compound: argument is __oomph_expr<T, U> (with U not equal to T) */


// simple expressions

template <class T, class Op>
class __oomph_expr<T, __oomph_unary_expr<__oomph_expr<T, T>, Op> >
{
private:
  typedef __oomph_expr<T, T> val_type;

  __oomph_unary_expr<val_type, Op> expr;
public:
  __oomph_expr(const val_type &val) : expr(val) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    unsigned long int = 0) const
  { Op::eval(p, expr.val.__get_mp()); }
  const val_type & get_val() const { return expr.val; }
  unsigned long int get_prec() const { return expr.val.get_prec(); }
};


// compound expressions

template <class T, class U, class Op>
class __oomph_expr<T, __oomph_unary_expr<__oomph_expr<T, U>, Op> >
{
private:
  typedef __oomph_expr<T, U> val_type;

  __oomph_unary_expr<val_type, Op> expr;
public:
  __oomph_expr(const val_type &val) : expr(val) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  { __oomph_expr<T, T> temp(expr.val); Op::eval(p, temp.__get_mp()); }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  { __oomph_expr<T, T> temp(expr.val, prec); Op::eval(p, temp.__get_mp()); }
  const val_type & get_val() const { return expr.val; }
  unsigned long int get_prec() const { return expr.val.get_prec(); }
};


/**************** Binary expressions ****************/
/* simple:
   - arguments are both mp*_class
   - one argument is mp*_class, one is a built-in type
   compound:
   - one is mp*_class, one is __oomph_expr<T, U>
   - one is __oomph_expr<T, U>, one is built-in
   - both arguments are __oomph_expr<...> */


// simple expressions

template <class T, class Op>
class __oomph_expr
<T, __oomph_binary_expr<__oomph_expr<T, T>, __oomph_expr<T, T>, Op> >
{
private:
  typedef __oomph_expr<T, T> val1_type;
  typedef __oomph_expr<T, T> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    unsigned long int = 0) const
  { Op::eval(p, expr.val1.__get_mp(), expr.val2.__get_mp()); }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const
  {
    mp_bitcnt_t prec1 = expr.val1.get_prec(),
      prec2 = expr.val2.get_prec();
    return (prec1 > prec2) ? prec1 : prec2;
  }
};


// simple expressions, T is a built-in numerical type

template <class T, class U, class Op>
class __oomph_expr<T, __oomph_binary_expr<__oomph_expr<T, T>, U, Op> >
{
private:
  typedef __oomph_expr<T, T> val1_type;
  typedef U val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    unsigned long int = 0) const
  { Op::eval(p, expr.val1.__get_mp(), expr.val2); }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const { return expr.val1.get_prec(); }
};

template <class T, class U, class Op>
class __oomph_expr<T, __oomph_binary_expr<U, __oomph_expr<T, T>, Op> >
{
private:
  typedef U val1_type;
  typedef __oomph_expr<T, T> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    unsigned long int = 0) const
  { Op::eval(p, expr.val1, expr.val2.__get_mp()); }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const { return expr.val2.get_prec(); }
};


// compound expressions, one argument is a subexpression

template <class T, class U, class V, class Op>
class __oomph_expr
<T, __oomph_binary_expr<__oomph_expr<T, T>, __oomph_expr<U, V>, Op> >
{
private:
  typedef __oomph_expr<T, T> val1_type;
  typedef __oomph_expr<U, V> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  {
    __oomph_expr<T, T> temp(expr.val2);
    Op::eval(p, expr.val1.__get_mp(), temp.__get_mp());
  }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  {
    __oomph_expr<T, T> temp(expr.val2, prec);
    Op::eval(p, expr.val1.__get_mp(), temp.__get_mp());
  }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const
  {
    mp_bitcnt_t prec1 = expr.val1.get_prec(),
      prec2 = expr.val2.get_prec();
    return (prec1 > prec2) ? prec1 : prec2;
  }
};

template <class T, class U, class V, class Op>
class __oomph_expr
<T, __oomph_binary_expr<__oomph_expr<U, V>, __oomph_expr<T, T>, Op> >
{
private:
  typedef __oomph_expr<U, V> val1_type;
  typedef __oomph_expr<T, T> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  {
    __oomph_expr<T, T> temp(expr.val1);
    Op::eval(p, temp.__get_mp(), expr.val2.__get_mp());
  }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  {
    __oomph_expr<T, T> temp(expr.val1, prec);
    Op::eval(p, temp.__get_mp(), expr.val2.__get_mp());
  }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const
  {
    mp_bitcnt_t prec1 = expr.val1.get_prec(),
      prec2 = expr.val2.get_prec();
    return (prec1 > prec2) ? prec1 : prec2;
  }
};

template <class T, class U, class Op>
class __oomph_expr
<T, __oomph_binary_expr<__oomph_expr<T, T>, __oomph_expr<T, U>, Op> >
{
private:
  typedef __oomph_expr<T, T> val1_type;
  typedef __oomph_expr<T, U> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  {
    __oomph_expr<T, T> temp(expr.val2);
    Op::eval(p, expr.val1.__get_mp(), temp.__get_mp());
  }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  {
    __oomph_expr<T, T> temp(expr.val2, prec);
    Op::eval(p, expr.val1.__get_mp(), temp.__get_mp());
  }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const
  {
    mp_bitcnt_t prec1 = expr.val1.get_prec(),
      prec2 = expr.val2.get_prec();
    return (prec1 > prec2) ? prec1 : prec2;
  }
};

template <class T, class U, class Op>
class __oomph_expr
<T, __oomph_binary_expr<__oomph_expr<T, U>, __oomph_expr<T, T>, Op> >
{
private:
  typedef __oomph_expr<T, U> val1_type;
  typedef __oomph_expr<T, T> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  {
    __oomph_expr<T, T> temp(expr.val1);
    Op::eval(p, temp.__get_mp(), expr.val2.__get_mp());
  }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  {
    __oomph_expr<T, T> temp(expr.val1, prec);
    Op::eval(p, temp.__get_mp(), expr.val2.__get_mp());
  }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const
  {
    mp_bitcnt_t prec1 = expr.val1.get_prec(),
      prec2 = expr.val2.get_prec();
    return (prec1 > prec2) ? prec1 : prec2;
  }
};


// one argument is a subexpression, one is a built-in

template <class T, class U, class V, class Op>
class __oomph_expr<T, __oomph_binary_expr<__oomph_expr<T, U>, V, Op> >
{
private:
  typedef __oomph_expr<T, U> val1_type;
  typedef V val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  {
    __oomph_expr<T, T> temp(expr.val1);
    Op::eval(p, temp.__get_mp(), expr.val2);
  }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  {
    __oomph_expr<T, T> temp(expr.val1, prec);
    Op::eval(p, temp.__get_mp(), expr.val2);
  }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const { return expr.val1.get_prec(); }
};

template <class T, class U, class V, class Op>
class __oomph_expr<T, __oomph_binary_expr<U, __oomph_expr<T, V>, Op> >
{
private:
  typedef U val1_type;
  typedef __oomph_expr<T, V> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  {
    __oomph_expr<T, T> temp(expr.val2);
    Op::eval(p, expr.val1, temp.__get_mp());
  }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  {
    __oomph_expr<T, T> temp(expr.val2, prec);
    Op::eval(p, expr.val1, temp.__get_mp());
  }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const { return expr.val2.get_prec(); }
};


// both arguments are subexpressions

template <class T, class U, class V, class W, class Op>
class __oomph_expr
<T, __oomph_binary_expr<__oomph_expr<T, U>, __oomph_expr<V, W>, Op> >
{
private:
  typedef __oomph_expr<T, U> val1_type;
  typedef __oomph_expr<V, W> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  {
    __oomph_expr<T, T> temp1(expr.val1), temp2(expr.val2);
    Op::eval(p, temp1.__get_mp(), temp2.__get_mp());
  }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  {
    __oomph_expr<T, T> temp1(expr.val1, prec), temp2(expr.val2, prec);
    Op::eval(p, temp1.__get_mp(), temp2.__get_mp());
  }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const
  {
    mp_bitcnt_t prec1 = expr.val1.get_prec(),
      prec2 = expr.val2.get_prec();
    return (prec1 > prec2) ? prec1 : prec2;
  }
};

template <class T, class U, class V, class W, class Op>
class __oomph_expr
<T, __oomph_binary_expr<__oomph_expr<U, V>, __oomph_expr<T, W>, Op> >
{
private:
  typedef __oomph_expr<U, V> val1_type;
  typedef __oomph_expr<T, W> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  {
    __oomph_expr<T, T> temp1(expr.val1), temp2(expr.val2);
    Op::eval(p, temp1.__get_mp(), temp2.__get_mp());
  }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  {
    __oomph_expr<T, T> temp1(expr.val1, prec), temp2(expr.val2, prec);
    Op::eval(p, temp1.__get_mp(), temp2.__get_mp());
  }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const
  {
    mp_bitcnt_t prec1 = expr.val1.get_prec(),
      prec2 = expr.val2.get_prec();
    return (prec1 > prec2) ? prec1 : prec2;
  }
};

template <class T, class U, class V, class Op>
class __oomph_expr
<T, __oomph_binary_expr<__oomph_expr<T, U>, __oomph_expr<T, V>, Op> >
{
private:
  typedef __oomph_expr<T, U> val1_type;
  typedef __oomph_expr<T, V> val2_type;

  __oomph_binary_expr<val1_type, val2_type, Op> expr;
public:
  __oomph_expr(const val1_type &val1, const val2_type &val2)
    : expr(val1, val2) { }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p) const
  {
    __oomph_expr<T, T> temp1(expr.val1), temp2(expr.val2);
    Op::eval(p, temp1.__get_mp(), temp2.__get_mp());
  }
  void eval(typename __oomph_resolve_expr<T>::ptr_type p,
	    mp_bitcnt_t prec) const
  {
    __oomph_expr<T, T> temp1(expr.val1, prec), temp2(expr.val2, prec);
    Op::eval(p, temp1.__get_mp(), temp2.__get_mp());
  }
  const val1_type & get_val1() const { return expr.val1; }
  const val2_type & get_val2() const { return expr.val2; }
  unsigned long int get_prec() const
  {
    mp_bitcnt_t prec1 = expr.val1.get_prec(),
      prec2 = expr.val2.get_prec();
    return (prec1 > prec2) ? prec1 : prec2;
  }
};


/**************** Special cases ****************/

/* Some operations (i.e., add and subtract) with mixed mpz/mpq arguments
   can be done directly without first converting the mpz to mpq.
   Appropriate specializations of __oomph_expr are required. */

/*
#define __GMPZQ_DEFINE_EXPR(eval_fun)                                       \
                                                                            \
template <>                                                                 \
class __oomph_expr<mpq_t, __oomph_binary_expr<mpz_class, mpq_class, eval_fun> > \
{                                                                           \
private:                                                                    \
  typedef mpz_class val1_type;                                              \
  typedef mpq_class val2_type;                                              \
                                                                            \
  __oomph_binary_expr<val1_type, val2_type, eval_fun> expr;                   \
public:                                                                     \
  __oomph_expr(const val1_type &val1, const val2_type &val2)                  \
    : expr(val1, val2) { }                                                  \
  void eval(mpq_ptr q) const                                                \
  { eval_fun::eval(q, expr.val1.get_mpz_t(), expr.val2.get_mpq_t()); }      \
  const val1_type & get_val1() const { return expr.val1; }                  \
  const val2_type & get_val2() const { return expr.val2; }                  \
  unsigned long int get_prec() const { return mpf_get_default_prec(); }     \
};                                                                          \
                                                                            \
template <>                                                                 \
class __oomph_expr<mpq_t, __oomph_binary_expr<mpq_class, mpz_class, eval_fun> > \
{                                                                           \
private:                                                                    \
  typedef mpq_class val1_type;                                              \
  typedef mpz_class val2_type;                                              \
                                                                            \
  __oomph_binary_expr<val1_type, val2_type, eval_fun> expr;                   \
public:                                                                     \
  __oomph_expr(const val1_type &val1, const val2_type &val2)                  \
    : expr(val1, val2) { }                                                  \
  void eval(mpq_ptr q) const                                                \
  { eval_fun::eval(q, expr.val1.get_mpq_t(), expr.val2.get_mpz_t()); }      \
  const val1_type & get_val1() const { return expr.val1; }                  \
  const val2_type & get_val2() const { return expr.val2; }                  \
  unsigned long int get_prec() const { return mpf_get_default_prec(); }     \
};                                                                          \
                                                                            \
template <class T>                                                          \
class __oomph_expr                                                            \
<mpq_t, __oomph_binary_expr<mpz_class, __oomph_expr<mpq_t, T>, eval_fun> >      \
{                                                                           \
private:                                                                    \
  typedef mpz_class val1_type;                                              \
  typedef __oomph_expr<mpq_t, T> val2_type;                                   \
                                                                            \
  __oomph_binary_expr<val1_type, val2_type, eval_fun> expr;                   \
public:                                                                     \
  __oomph_expr(const val1_type &val1, const val2_type &val2)                  \
    : expr(val1, val2) { }                                                  \
  void eval(mpq_ptr q) const                                                \
  {                                                                         \
    mpq_class temp(expr.val2);                                              \
    eval_fun::eval(q, expr.val1.get_mpz_t(), temp.get_mpq_t());             \
  }                                                                         \
  const val1_type & get_val1() const { return expr.val1; }                  \
  const val2_type & get_val2() const { return expr.val2; }                  \
  unsigned long int get_prec() const { return mpf_get_default_prec(); }     \
};                                                                          \
                                                                            \
template <class T>                                                          \
class __oomph_expr                                                            \
<mpq_t, __oomph_binary_expr<mpq_class, __oomph_expr<mpz_t, T>, eval_fun> >      \
{                                                                           \
private:                                                                    \
  typedef mpq_class val1_type;                                              \
  typedef __oomph_expr<mpz_t, T> val2_type;                                   \
                                                                            \
  __oomph_binary_expr<val1_type, val2_type, eval_fun> expr;                   \
public:                                                                     \
  __oomph_expr(const val1_type &val1, const val2_type &val2)                  \
    : expr(val1, val2) { }                                                  \
  void eval(mpq_ptr q) const                                                \
  {                                                                         \
    mpz_class temp(expr.val2);                                              \
    eval_fun::eval(q, expr.val1.get_mpq_t(), temp.get_mpz_t());             \
  }                                                                         \
  const val1_type & get_val1() const { return expr.val1; }                  \
  const val2_type & get_val2() const { return expr.val2; }                  \
  unsigned long int get_prec() const { return mpf_get_default_prec(); }     \
};                                                                          \
                                                                            \
template <class T>                                                          \
class __oomph_expr                                                            \
<mpq_t, __oomph_binary_expr<__oomph_expr<mpz_t, T>, mpq_class, eval_fun> >      \
{                                                                           \
private:                                                                    \
  typedef __oomph_expr<mpz_t, T> val1_type;                                   \
  typedef mpq_class val2_type;                                              \
                                                                            \
  __oomph_binary_expr<val1_type, val2_type, eval_fun> expr;                   \
public:                                                                     \
  __oomph_expr(const val1_type &val1, const val2_type &val2)                  \
    : expr(val1, val2) { }                                                  \
  void eval(mpq_ptr q) const                                                \
  {                                                                         \
    mpz_class temp(expr.val1);                                              \
    eval_fun::eval(q, temp.get_mpz_t(), expr.val2.get_mpq_t());             \
  }                                                                         \
  const val1_type & get_val1() const { return expr.val1; }                  \
  const val2_type & get_val2() const { return expr.val2; }                  \
  unsigned long int get_prec() const { return mpf_get_default_prec(); }     \
};                                                                          \
                                                                            \
template <class T>                                                          \
class __oomph_expr                                                            \
<mpq_t, __oomph_binary_expr<__oomph_expr<mpq_t, T>, mpz_class, eval_fun> >      \
{                                                                           \
private:                                                                    \
  typedef __oomph_expr<mpq_t, T> val1_type;                                   \
  typedef mpz_class val2_type;                                              \
                                                                            \
  __oomph_binary_expr<val1_type, val2_type, eval_fun> expr;                   \
public:                                                                     \
  __oomph_expr(const val1_type &val1, const val2_type &val2)                  \
    : expr(val1, val2) { }                                                  \
  void eval(mpq_ptr q) const                                                \
  {                                                                         \
    mpq_class temp(expr.val1);                                              \
    eval_fun::eval(q, temp.get_mpq_t(), expr.val2.get_mpz_t());             \
  }                                                                         \
  const val1_type & get_val1() const { return expr.val1; }                  \
  const val2_type & get_val2() const { return expr.val2; }                  \
  unsigned long int get_prec() const { return mpf_get_default_prec(); }     \
};                                                                          \
                                                                            \
template <class T, class U>                                                 \
class __oomph_expr<mpq_t, __oomph_binary_expr                                   \
<__oomph_expr<mpz_t, T>, __oomph_expr<mpq_t, U>, eval_fun> >                    \
{                                                                           \
private:                                                                    \
  typedef __oomph_expr<mpz_t, T> val1_type;                                   \
  typedef __oomph_expr<mpq_t, U> val2_type;                                   \
                                                                            \
  __oomph_binary_expr<val1_type, val2_type, eval_fun> expr;                   \
public:                                                                     \
  __oomph_expr(const val1_type &val1, const val2_type &val2)                  \
    : expr(val1, val2) { }                                                  \
  void eval(mpq_ptr q) const                                                \
  {                                                                         \
    mpz_class temp1(expr.val1);                                             \
    mpq_class temp2(expr.val2);                                             \
    eval_fun::eval(q, temp1.get_mpz_t(), temp2.get_mpq_t());                \
  }                                                                         \
  const val1_type & get_val1() const { return expr.val1; }                  \
  const val2_type & get_val2() const { return expr.val2; }                  \
  unsigned long int get_prec() const { return mpf_get_default_prec(); }     \
};                                                                          \
                                                                            \
template <class T, class U>                                                 \
class __oomph_expr<mpq_t, __oomph_binary_expr                                   \
<__oomph_expr<mpq_t, T>, __oomph_expr<mpz_t, U>, eval_fun> >                    \
{                                                                           \
private:                                                                    \
  typedef __oomph_expr<mpq_t, T> val1_type;                                   \
  typedef __oomph_expr<mpz_t, U> val2_type;                                   \
                                                                            \
  __oomph_binary_expr<val1_type, val2_type, eval_fun> expr;                   \
public:                                                                     \
  __oomph_expr(const val1_type &val1, const val2_type &val2)                  \
    : expr(val1, val2) { }                                                  \
  void eval(mpq_ptr q) const                                                \
  {                                                                         \
    mpq_class temp1(expr.val1);                                             \
    mpz_class temp2(expr.val2);                                             \
    eval_fun::eval(q, temp1.get_mpq_t(), temp2.get_mpz_t());                \
  }                                                                         \
  const val1_type & get_val1() const { return expr.val1; }                  \
  const val2_type & get_val2() const { return expr.val2; }                  \
  unsigned long int get_prec() const { return mpf_get_default_prec(); }     \
};


__GMPZQ_DEFINE_EXPR(__oomph_binary_plus)
__GMPZQ_DEFINE_EXPR(__oomph_binary_minus)
*/

/**************** Macros for defining functions ****************/
/* Results of operators and functions are instances of __oomph_expr<T, U>.
   T determines the numerical type of the expression: it can be either
   mpz_t, mpq_t, or mpf_t.  When the arguments of a binary
   expression have different numerical types, __oomph_resolve_expr is used
   to determine the "larger" type.
   U is either __oomph_unary_expr<V, Op> or __oomph_binary_expr<V, W, Op>,
   where V and W are the arguments' types -- they can in turn be
   expressions, thus allowing to build compound expressions to any
   degree of complexity.
   Op is a function object that must have an eval() method accepting
   appropriate arguments.
   Actual evaluation of a __oomph_expr<T, U> object is done when it gets
   assigned to an mp*_class ("lazy" evaluation): this is done by calling
   its eval() method. */


// non-member unary operators and functions

#define __GMP_DEFINE_UNARY_FUNCTION(fun, eval_fun)                           \
                                                                             \
template <class T, class U>                                                  \
inline __oomph_expr<T, __oomph_unary_expr<__oomph_expr<T, U>, eval_fun> >          \
fun(const __oomph_expr<T, U> &expr)                                            \
{                                                                            \
  return __oomph_expr<T, __oomph_unary_expr<__oomph_expr<T, U>, eval_fun> >(expr); \
}

#define __GMP_DEFINE_UNARY_TYPE_FUNCTION(type, fun, eval_fun) \
                                                              \
template <class T, class U>                                   \
inline type fun(const __oomph_expr<T, U> &expr)                 \
{                                                             \
  typename __oomph_resolve_temp<T, T, U>::temp_type temp(expr); \
  return eval_fun::eval(temp.__get_mp());                     \
}


// non-member binary operators and functions

#define __GMPP_DEFINE_BINARY_FUNCTION(fun, eval_fun)                   \
                                                                       \
template <class T, class U, class V, class W>                          \
inline __oomph_expr<typename __oomph_resolve_expr<T, V>::value_type,       \
__oomph_binary_expr<__oomph_expr<T, U>, __oomph_expr<V, W>, eval_fun> >      \
fun(const __oomph_expr<T, U> &expr1, const __oomph_expr<V, W> &expr2)      \
{                                                                      \
  return __oomph_expr<typename __oomph_resolve_expr<T, V>::value_type,     \
     __oomph_binary_expr<__oomph_expr<T, U>, __oomph_expr<V, W>, eval_fun> > \
    (expr1, expr2);                                                    \
}

#define __GMPNN_DEFINE_BINARY_FUNCTION(fun, eval_fun, type, bigtype)       \
                                                                           \
template <class T, class U>                                                \
inline __oomph_expr                                                          \
<T, __oomph_binary_expr<__oomph_expr<T, U>, bigtype, eval_fun> >               \
fun(const __oomph_expr<T, U> &expr, type t)                                  \
{                                                                          \
  return __oomph_expr                                                        \
    <T, __oomph_binary_expr<__oomph_expr<T, U>, bigtype, eval_fun> >(expr, t); \
}                                                                          \
                                                                           \
template <class T, class U>                                                \
inline __oomph_expr                                                          \
<T, __oomph_binary_expr<bigtype, __oomph_expr<T, U>, eval_fun> >               \
fun(type t, const __oomph_expr<T, U> &expr)                                  \
{                                                                          \
  return __oomph_expr                                                        \
    <T, __oomph_binary_expr<bigtype, __oomph_expr<T, U>, eval_fun> >(t, expr); \
}

#define __GMPNS_DEFINE_BINARY_FUNCTION(fun, eval_fun, type)          \
__GMPNN_DEFINE_BINARY_FUNCTION(fun, eval_fun, type, signed long int)

#define __GMPNU_DEFINE_BINARY_FUNCTION(fun, eval_fun, type)            \
__GMPNN_DEFINE_BINARY_FUNCTION(fun, eval_fun, type, unsigned long int)

#define __GMPND_DEFINE_BINARY_FUNCTION(fun, eval_fun, type) \
__GMPNN_DEFINE_BINARY_FUNCTION(fun, eval_fun, type, double)

#define __GMPNLD_DEFINE_BINARY_FUNCTION(fun, eval_fun, type)     \
__GMPNN_DEFINE_BINARY_FUNCTION(fun, eval_fun, type, long double)

#define __GMPN_DEFINE_BINARY_FUNCTION(fun, eval_fun)              \
__GMPNS_DEFINE_BINARY_FUNCTION(fun, eval_fun, signed char)        \
__GMPNU_DEFINE_BINARY_FUNCTION(fun, eval_fun, unsigned char)      \
__GMPNS_DEFINE_BINARY_FUNCTION(fun, eval_fun, signed int)         \
__GMPNU_DEFINE_BINARY_FUNCTION(fun, eval_fun, unsigned int)       \
__GMPNS_DEFINE_BINARY_FUNCTION(fun, eval_fun, signed short int)   \
__GMPNU_DEFINE_BINARY_FUNCTION(fun, eval_fun, unsigned short int) \
__GMPNS_DEFINE_BINARY_FUNCTION(fun, eval_fun, signed long int)    \
__GMPNU_DEFINE_BINARY_FUNCTION(fun, eval_fun, unsigned long int)  \
__GMPND_DEFINE_BINARY_FUNCTION(fun, eval_fun, float)              \
__GMPND_DEFINE_BINARY_FUNCTION(fun, eval_fun, double)             \
__GMPNLD_DEFINE_BINARY_FUNCTION(fun, eval_fun, long double)

#define __GMP_DEFINE_BINARY_FUNCTION(fun, eval_fun) \
__GMPP_DEFINE_BINARY_FUNCTION(fun, eval_fun)        \
__GMPN_DEFINE_BINARY_FUNCTION(fun, eval_fun)


#define __GMP_DEFINE_BINARY_FUNCTION_UI(fun, eval_fun)                 \
                                                                       \
template <class T, class U>                                            \
inline __oomph_expr                                                      \
<T, __oomph_binary_expr<__oomph_expr<T, U>, unsigned long int, eval_fun> > \
fun(const __oomph_expr<T, U> &expr, unsigned long int l)                 \
{                                                                      \
  return __oomph_expr<T, __oomph_binary_expr                               \
    <__oomph_expr<T, U>, unsigned long int, eval_fun> >(expr, l);        \
}


#define __GMPP_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun)         \
                                                                        \
template <class T, class U, class V, class W>                           \
inline type fun(const __oomph_expr<T, U> &expr1,                          \
		const __oomph_expr<V, W> &expr2)                          \
{                                                                       \
  typedef typename __oomph_resolve_expr<T, V>::value_type eval_type;      \
  typename __oomph_resolve_temp<eval_type, T, U>::temp_type temp1(expr1); \
  typename __oomph_resolve_temp<eval_type, V, W>::temp_type temp2(expr2); \
  return eval_fun::eval(temp1.__get_mp(), temp2.__get_mp());            \
}

#define __GMPNN_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun,   \
					    type2, bigtype)        \
                                                                   \
template <class T, class U>                                        \
inline type fun(const __oomph_expr<T, U> &expr, type2 t)             \
{                                                                  \
  typename __oomph_resolve_temp<T, T, U>::temp_type temp(expr);      \
  return eval_fun::eval(temp.__get_mp(), static_cast<bigtype>(t)); \
}                                                                  \
                                                                   \
template <class T, class U>                                        \
inline type fun(type2 t, const __oomph_expr<T, U> &expr)             \
{                                                                  \
  typename __oomph_resolve_temp<T, T, U>::temp_type temp(expr);      \
  return eval_fun::eval(static_cast<bigtype>(t), temp.__get_mp()); \
}

#define __GMPNS_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, type2) \
__GMPNN_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun,                \
				    type2, signed long int)

#define __GMPNU_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, type2) \
__GMPNN_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun,                \
				    type2, unsigned long int)

#define __GMPND_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, type2) \
__GMPNN_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, type2, double)

#define __GMPNLD_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, type2)     \
__GMPNN_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, type2, long double)

#define __GMPN_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun)              \
__GMPNS_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, signed char)        \
__GMPNU_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, unsigned char)      \
__GMPNS_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, signed int)         \
__GMPNU_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, unsigned int)       \
__GMPNS_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, signed short int)   \
__GMPNU_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, unsigned short int) \
__GMPNS_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, signed long int)    \
__GMPNU_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, unsigned long int)  \
__GMPND_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, float)              \
__GMPND_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, double)             \
__GMPNLD_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun, long double)

#define __GMP_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun) \
__GMPP_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun)        \
__GMPN_DEFINE_BINARY_TYPE_FUNCTION(type, fun, eval_fun)


// member operators

#define __GMPP_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun)                 \
                                                                             \
template <class T, class U>                                                  \
inline type##_class & type##_class::fun(const __oomph_expr<T, U> &expr)        \
{                                                                            \
  __oomph_set_expr(mp, __oomph_expr<type##_t, __oomph_binary_expr                  \
		 <type##_class, __oomph_expr<T, U>, eval_fun> >(*this, expr)); \
  return *this;                                                              \
}

#define __GMPNN_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun,    \
					 type2, bigtype)         \
                                                                 \
inline type##_class & type##_class::fun(type2 t)                 \
{                                                                \
  __oomph_set_expr(mp, __oomph_expr<type##_t, __oomph_binary_expr      \
		 <type##_class, bigtype, eval_fun> >(*this, t)); \
  return *this;                                                  \
}

#define __GMPNS_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, type2) \
__GMPNN_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun,                \
				 type2, signed long int)

#define __GMPNU_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, type2) \
__GMPNN_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun,                \
				 type2, unsigned long int)

#define __GMPND_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, type2) \
__GMPNN_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, type2, double)

#define __GMPNLD_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, type2)     \
__GMPNN_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, type2, long double)

#define __GMPN_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun)              \
__GMPNS_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, signed char)        \
__GMPNU_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, unsigned char)      \
__GMPNS_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, signed int)         \
__GMPNU_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, unsigned int)       \
__GMPNS_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, signed short int)   \
__GMPNU_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, unsigned short int) \
__GMPNS_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, signed long int)    \
__GMPNU_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, unsigned long int)  \
__GMPND_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, float)              \
__GMPND_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, double)             \
/* __GMPNLD_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun, long double) */

#define __GMP_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun) \
__GMPP_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun)        \
__GMPN_DEFINE_COMPOUND_OPERATOR(type, fun, eval_fun)

#define __GMPZ_DEFINE_COMPOUND_OPERATOR(fun, eval_fun) \
__GMP_DEFINE_COMPOUND_OPERATOR(mpz, fun, eval_fun)

#define __GMPQ_DEFINE_COMPOUND_OPERATOR(fun, eval_fun) \
__GMP_DEFINE_COMPOUND_OPERATOR(mpq, fun, eval_fun)

#define __GMPF_DEFINE_COMPOUND_OPERATOR(fun, eval_fun) \
__GMP_DEFINE_COMPOUND_OPERATOR(mpf, fun, eval_fun)

#define __GMPFI_DEFINE_COMPOUND_OPERATOR(fun, eval_fun) \
__GMP_DEFINE_COMPOUND_OPERATOR(mpfi, fun, eval_fun)


#define __GMP_DEFINE_COMPOUND_OPERATOR_UI(type, fun, eval_fun)  \
                                                                \
inline type##_class & type##_class::fun(unsigned long int l)    \
{                                                               \
  __oomph_set_expr(mp, __oomph_expr<type##_t, __oomph_binary_expr     \
    <type##_class, unsigned long int, eval_fun> >(*this, l));   \
  return *this;                                                 \
}

#define __GMPZ_DEFINE_COMPOUND_OPERATOR_UI(fun, eval_fun) \
__GMP_DEFINE_COMPOUND_OPERATOR_UI(mpz, fun, eval_fun)

#define __GMPQ_DEFINE_COMPOUND_OPERATOR_UI(fun, eval_fun) \
__GMP_DEFINE_COMPOUND_OPERATOR_UI(mpq, fun, eval_fun)

#define __GMPF_DEFINE_COMPOUND_OPERATOR_UI(fun, eval_fun) \
__GMP_DEFINE_COMPOUND_OPERATOR_UI(mpf, fun, eval_fun)

#define __GMPFI_DEFINE_COMPOUND_OPERATOR_UI(fun, eval_fun) \
__GMP_DEFINE_COMPOUND_OPERATOR_UI(mpfi, fun, eval_fun)


#define __GMP_DEFINE_INCREMENT_OPERATOR(type, fun, eval_fun) \
                                                             \
inline type##_class & type##_class::fun()                    \
{                                                            \
  eval_fun::eval(mp);                                        \
  return *this;                                              \
}                                                            \
                                                             \
inline type##_class type##_class::fun(int)                   \
{                                                            \
  type##_class temp(*this);                                  \
  eval_fun::eval(mp);                                        \
  return temp;                                               \
}

#define __GMPZ_DEFINE_INCREMENT_OPERATOR(fun, eval_fun) \
__GMP_DEFINE_INCREMENT_OPERATOR(mpz, fun, eval_fun)

#define __GMPQ_DEFINE_INCREMENT_OPERATOR(fun, eval_fun) \
__GMP_DEFINE_INCREMENT_OPERATOR(mpq, fun, eval_fun)

#define __GMPF_DEFINE_INCREMENT_OPERATOR(fun, eval_fun) \
__GMP_DEFINE_INCREMENT_OPERATOR(mpf, fun, eval_fun)

#define __GMPFI_DEFINE_INCREMENT_OPERATOR(fun, eval_fun) \
__GMP_DEFINE_INCREMENT_OPERATOR(mpfi, fun, eval_fun)


/**************** Arithmetic operators and functions ****************/

// non-member operators and functions

__GMP_DEFINE_UNARY_FUNCTION(operator+, __oomph_unary_plus)
__GMP_DEFINE_UNARY_FUNCTION(operator-, __oomph_unary_minus)

__GMP_DEFINE_BINARY_FUNCTION(operator+, __oomph_binary_plus)
__GMP_DEFINE_BINARY_FUNCTION(operator-, __oomph_binary_minus)
__GMP_DEFINE_BINARY_FUNCTION(operator*, __oomph_binary_multiplies)
__GMP_DEFINE_BINARY_FUNCTION(operator/, __oomph_binary_divides)

__GMP_DEFINE_BINARY_TYPE_FUNCTION(bool, operator==, __oomph_binary_equal)
__GMP_DEFINE_BINARY_TYPE_FUNCTION(bool, operator!=, __oomph_binary_not_equal)
__GMP_DEFINE_BINARY_TYPE_FUNCTION(bool, operator<, __oomph_binary_less)
__GMP_DEFINE_BINARY_TYPE_FUNCTION(bool, operator<=, __oomph_binary_less_equal)
__GMP_DEFINE_BINARY_TYPE_FUNCTION(bool, operator>, __oomph_binary_greater)
__GMP_DEFINE_BINARY_TYPE_FUNCTION(bool, operator>=, \
                                  __oomph_binary_greater_equal)

//__GMP_DEFINE_UNARY_FUNCTION(trunc, __oomph_trunc_function)
//__GMP_DEFINE_UNARY_FUNCTION(floor, __oomph_floor_function)
//__GMP_DEFINE_UNARY_FUNCTION(ceil, __oomph_ceil_function)
__GMP_DEFINE_UNARY_FUNCTION(sqr, __oomph_sqr_function)
__GMP_DEFINE_UNARY_FUNCTION(inv, __oomph_inv_function)
__GMP_DEFINE_UNARY_FUNCTION(sqrt, __oomph_sqrt_function)
__GMP_DEFINE_UNARY_FUNCTION(cbrt, __oomph_cbrt_function)
__GMP_DEFINE_UNARY_FUNCTION(abs, __oomph_abs_function)
__GMP_DEFINE_UNARY_FUNCTION(log, __oomph_log_function)
__GMP_DEFINE_UNARY_FUNCTION(log1p, __oomph_log1p_function)
__GMP_DEFINE_UNARY_FUNCTION(log2, __oomph_log2_function)
__GMP_DEFINE_UNARY_FUNCTION(log10, __oomph_log10_function)
__GMP_DEFINE_UNARY_FUNCTION(exp, __oomph_exp_function)
__GMP_DEFINE_UNARY_FUNCTION(exp2, __oomph_exp2_function)
__GMP_DEFINE_UNARY_FUNCTION(expm1, __oomph_expm1_function)
__GMP_DEFINE_UNARY_FUNCTION(cos, __oomph_cos_function)
__GMP_DEFINE_UNARY_FUNCTION(sin, __oomph_sin_function)
__GMP_DEFINE_UNARY_FUNCTION(tan, __oomph_tan_function)
__GMP_DEFINE_UNARY_FUNCTION(acos, __oomph_acos_function)
__GMP_DEFINE_UNARY_FUNCTION(asin, __oomph_asin_function)
__GMP_DEFINE_UNARY_FUNCTION(atan, __oomph_atan_function)
__GMP_DEFINE_UNARY_FUNCTION(sec, __oomph_sec_function)
__GMP_DEFINE_UNARY_FUNCTION(csc, __oomph_csc_function)
__GMP_DEFINE_UNARY_FUNCTION(cot, __oomph_cot_function)
__GMP_DEFINE_UNARY_FUNCTION(cosh, __oomph_cosh_function)
__GMP_DEFINE_UNARY_FUNCTION(sinh, __oomph_sinh_function)
__GMP_DEFINE_UNARY_FUNCTION(tanh, __oomph_tanh_function)
__GMP_DEFINE_UNARY_FUNCTION(acosh, __oomph_acosh_function)
__GMP_DEFINE_UNARY_FUNCTION(asinh, __oomph_asinh_function)
__GMP_DEFINE_UNARY_FUNCTION(atanh, __oomph_atanh_function)
__GMP_DEFINE_UNARY_FUNCTION(sech, __oomph_sech_function)
__GMP_DEFINE_UNARY_FUNCTION(csch, __oomph_csch_function)
__GMP_DEFINE_UNARY_FUNCTION(coth, __oomph_coth_function)
__GMP_DEFINE_BINARY_FUNCTION(hypot, __oomph_hypot_function)

__GMP_DEFINE_UNARY_TYPE_FUNCTION(int, sgn, __oomph_sgn_function)
__GMP_DEFINE_BINARY_TYPE_FUNCTION(int, cmp, __oomph_cmp_function)

// member operators for mpfi_class

__GMPFI_DEFINE_COMPOUND_OPERATOR(operator+=, __oomph_binary_plus)
__GMPFI_DEFINE_COMPOUND_OPERATOR(operator-=, __oomph_binary_minus)
__GMPFI_DEFINE_COMPOUND_OPERATOR(operator*=, __oomph_binary_multiplies)
__GMPFI_DEFINE_COMPOUND_OPERATOR(operator/=, __oomph_binary_divides)

__GMPFI_DEFINE_INCREMENT_OPERATOR(operator++, __oomph_unary_increment)
__GMPFI_DEFINE_INCREMENT_OPERATOR(operator--, __oomph_unary_decrement)

/**************** Class wrapper for gmp_randstate_t ****************/
/*
class __oomph_urandomb_value { };
class __oomph_urandomm_value { };

template <>
class __oomph_expr<mpz_t, __oomph_urandomb_value>
{
private:
  __oomph_randstate_struct *state;
  unsigned long int bits;
public:
  __oomph_expr(gmp_randstate_t s, unsigned long int l) : state(s), bits(l) { }
  void eval(mpz_ptr z) const { __oomph_rand_function::eval(z, state, bits); }
  unsigned long int get_prec() const { return mpf_get_default_prec(); }
};

template <>
class __oomph_expr<mpz_t, __oomph_urandomm_value>
{
private:
  __oomph_randstate_struct *state;
  mpz_class range;
public:
  __oomph_expr(gmp_randstate_t s, const mpz_class &z) : state(s), range(z) { }
  void eval(mpz_ptr z) const
  { __oomph_rand_function::eval(z, state, range.get_mpz_t()); }
  unsigned long int get_prec() const { return mpf_get_default_prec(); }
};

template <>
class __oomph_expr<mpf_t, __oomph_urandomb_value>
{
private:
  __oomph_randstate_struct *state;
  unsigned long int bits;
public:
  __oomph_expr(gmp_randstate_t s, unsigned long int l) : state(s), bits(l) { }
  void eval(mpf_ptr f, mp_bitcnt_t prec) const
  { __oomph_rand_function::eval(f, state, (bits>0) ? get_prec() : prec); }
  unsigned long int get_prec() const
  {
    if (bits == 0)
      return mpf_get_default_prec();
    else
      return bits;
  }
};

extern "C" {
  typedef void __oomph_randinit_default_t (gmp_randstate_t);
  typedef void __oomph_randinit_lc_2exp_t (gmp_randstate_t, mpz_srcptr, unsigned long int, unsigned long int);
  typedef int __oomph_randinit_lc_2exp_size_t (gmp_randstate_t, unsigned long int);
}

class gmp_randclass
{
private:
  gmp_randstate_t state;

  // copy construction and assignment not allowed
  gmp_randclass(const gmp_randclass &);
  void operator=(const gmp_randclass &);
public:
  // constructors and destructor
  gmp_randclass(gmp_randalg_t alg, unsigned long int size)
  {
    switch (alg)
      {
      case GMP_RAND_ALG_LC: // no other cases for now
      default:
	gmp_randinit(state, alg, size);
	break;
      }
  }

  // gmp_randinit_default
  gmp_randclass(__oomph_randinit_default_t* f) { f(state); }

  // gmp_randinit_lc_2exp
  gmp_randclass(__oomph_randinit_lc_2exp_t* f,
		mpz_class z, unsigned long int l1, unsigned long int l2)
  { f(state, z.get_mpz_t(), l1, l2); }

  // gmp_randinit_lc_2exp_size
  gmp_randclass(__oomph_randinit_lc_2exp_size_t* f,
		unsigned long int size)
  {
    if (f (state, size) == 0)
      throw std::length_error ("gmp_randinit_lc_2exp_size");
  }

  ~gmp_randclass() { gmp_randclear(state); }

  // initialize
  void seed(); // choose a random seed some way (?)
  void seed(unsigned long int s) { gmp_randseed_ui(state, s); }
  void seed(const mpz_class &z) { gmp_randseed(state, z.get_mpz_t()); }

  // get random number
  __oomph_expr<mpz_t, __oomph_urandomb_value> get_z_bits(unsigned long int l)
  { return __oomph_expr<mpz_t, __oomph_urandomb_value>(state, l); }
  __oomph_expr<mpz_t, __oomph_urandomb_value> get_z_bits(const mpz_class &z)
  { return get_z_bits(z.get_ui()); }

  __oomph_expr<mpz_t, __oomph_urandomm_value> get_z_range(const mpz_class &z)
  { return __oomph_expr<mpz_t, __oomph_urandomm_value>(state, z); }

  __oomph_expr<mpf_t, __oomph_urandomb_value> get_f(mp_bitcnt_t prec = 0)
  { return __oomph_expr<mpf_t, __oomph_urandomb_value>(state, prec); }
};
*/

/**************** #undef all private macros ****************/

#undef __GMPP_DECLARE_COMPOUND_OPERATOR
#undef __GMPN_DECLARE_COMPOUND_OPERATOR
#undef __GMP_DECLARE_COMPOUND_OPERATOR
#undef __GMP_DECLARE_COMPOUND_OPERATOR_UI
#undef __GMP_DECLARE_INCREMENT_OPERATOR

#undef __GMPZQ_DEFINE_EXPR

#undef __GMP_DEFINE_UNARY_FUNCTION
#undef __GMP_DEFINE_UNARY_TYPE_FUNCTION

#undef __GMPP_DEFINE_BINARY_FUNCTION
#undef __GMPNN_DEFINE_BINARY_FUNCTION
#undef __GMPNS_DEFINE_BINARY_FUNCTION
#undef __GMPNU_DEFINE_BINARY_FUNCTION
#undef __GMPND_DEFINE_BINARY_FUNCTION
#undef __GMPNLD_DEFINE_BINARY_FUNCTION
#undef __GMPN_DEFINE_BINARY_FUNCTION
#undef __GMP_DEFINE_BINARY_FUNCTION

#undef __GMP_DEFINE_BINARY_FUNCTION_UI

#undef __GMPP_DEFINE_BINARY_TYPE_FUNCTION
#undef __GMPNN_DEFINE_BINARY_TYPE_FUNCTION
#undef __GMPNS_DEFINE_BINARY_TYPE_FUNCTION
#undef __GMPNU_DEFINE_BINARY_TYPE_FUNCTION
#undef __GMPND_DEFINE_BINARY_TYPE_FUNCTION
#undef __GMPNLD_DEFINE_BINARY_TYPE_FUNCTION
#undef __GMPN_DEFINE_BINARY_TYPE_FUNCTION
#undef __GMP_DEFINE_BINARY_TYPE_FUNCTION

#undef __GMPZ_DEFINE_COMPOUND_OPERATOR
#undef __GMPZN_DEFINE_COMPOUND_OPERATOR
#undef __GMPZNN_DEFINE_COMPOUND_OPERATOR
#undef __GMPZNS_DEFINE_COMPOUND_OPERATOR
#undef __GMPZNU_DEFINE_COMPOUND_OPERATOR
#undef __GMPZND_DEFINE_COMPOUND_OPERATOR
#undef __GMPZNLD_DEFINE_COMPOUND_OPERATOR

#undef __GMPP_DEFINE_COMPOUND_OPERATOR
#undef __GMPNN_DEFINE_COMPOUND_OPERATOR
#undef __GMPNS_DEFINE_COMPOUND_OPERATOR
#undef __GMPNU_DEFINE_COMPOUND_OPERATOR
#undef __GMPND_DEFINE_COMPOUND_OPERATOR
#undef __GMPNLD_DEFINE_COMPOUND_OPERATOR
#undef __GMPN_DEFINE_COMPOUND_OPERATOR
#undef __GMP_DEFINE_COMPOUND_OPERATOR

#undef __GMPQ_DEFINE_COMPOUND_OPERATOR
#undef __GMPF_DEFINE_COMPOUND_OPERATOR

#undef __GMP_DEFINE_COMPOUND_OPERATOR_UI
#undef __GMPZ_DEFINE_COMPOUND_OPERATOR_UI
#undef __GMPQ_DEFINE_COMPOUND_OPERATOR_UI
#undef __GMPF_DEFINE_COMPOUND_OPERATOR_UI

#undef __GMP_DEFINE_INCREMENT_OPERATOR
#undef __GMPZ_DEFINE_INCREMENT_OPERATOR
#undef __GMPQ_DEFINE_INCREMENT_OPERATOR
#undef __GMPF_DEFINE_INCREMENT_OPERATOR

#endif //HPP_INTERVAL
