#include "interval.hpp"

/**************** mpf_class -- wrapper for mpf_t ****************/
/*
template <>
class __oomph_expr<mpfr_t, mpfr_t>
{
public:
  mpfr_prec_t get_prec () const { return mpfr_get_prec(mp); }

  void set_prec ( mpfr_prec_t prec ) { mpfr_set_prec(mp, prec); }

  void set_prec_raw ( mpfr_prec_t prec ) { mpfr_set_prec_raw(mp, prec); }

  // constructors and destructor
  __oomph_expr () : mp( new mpfr_t ) { mpfr_init(mp); }

  __oomph_expr ( __oomph_expr const& f ) : mp( new mpfr_t )
  { mpfr_init2(mp, f.get_prec()); mpfr_set(mp, f.mp); }

  __oomph_expr ( __oomph_expr && f ) : mp( f.mp )
  { f.mp = nullptr; }

  __oomph_expr ( __oomph_expr const& f, mpfr_prec_t prec ) : mp( new mpfr_t )
  { mpfr_init2(mp, prec); mpfr_set(mp, f.mp); }

  __oomph_expr ( __oomph_expr && f, mpfr_prec_t prec ) : mp( f.mp )
  { mpfr_set_prec(mp, prec); f.mp = nullptr; }

  template <class T, class U>
  __oomph_expr( __oomph_expr<T, U> const& expr ) : mp( new mpfr_t )
  { mpfr_init2(mp, expr.get_prec()); __oomph_set_expr(mp, expr); }

  template <class T, class U>
  __oomph_expr( __oomph_expr<T, U> const& expr, mpfr_prec_t prec ) : mp( new mpfr_t )
  { mpfr_init2(mp, prec); __oomph_set_expr(mp, expr); }

  template <typename T>
  __oomph_expr ( std::enable_if<std::is_integral<T>::value && std::is_signed<T>::value,T>::value n ) : mp( new mpfr_t )
  { mpfr_init_set_si(mp, n); }

  template <typename T>
  __oomph_expr ( std::enable_if<std::is_integral<T>::value && std::is_signed<T>::value,T>::value n, mpfr_prec_t prec ) : mp( new mpfr_t )
  { mpfr_init2(mp, prec); mpfr_set_si(mp, n); }

  template <typename T>
  __oomph_expr ( std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value,T>::value n ) : mp( new mpfr_t )
  { mpfr_init_set_ui(mp, n); }

  template <typename T>
  __oomph_expr ( std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value,T>::value n, mpfr_prec_t prec ) : mp( new mpfr_t )
  { mpfr_init2(mp, prec); mpfr_set_ui(mp, n); }

  template <typename T>
  __oomph_expr( std::enable_if<std::is_floating_point<T>::value,T>::value f ) : mp( new mpfr_t )
  { mpfr_init_set_d(mp, f); }

  template <typename T>
  __oomph_expr( std::enable_if<std::is_floating_point<T>::value,T>::value f, mpfr_prec_t prec) : mp( new mpfr_t )
  { mpfr_init2(mp, prec); mpfr_set_d(mp, f); }

  explicit __oomph_expr( char const* s ) : mp( new mpfr_t )
  {
    if (mpfr_init_set_str (mp, s, 0) != 0)
      {
        mpfr_clear (mp);
        throw std::invalid_argument ("mpfr_set_str");
      }
  }

  __oomph_expr( char const* s, mpfr_prec_t prec, int base = 0 ) : mp( new mpfr_t )
  {
    mpfr_init2(mp, prec);
    if (mpfr_set_str(mp, s, base) != 0)
      {
        mpfr_clear (mp);
        throw std::invalid_argument ("mpfr_set_str");
      }
  }

  explicit __oomph_expr( std::string const& s ) : mp( new mpfr_t )
  {
    if (mpfr_init_set_str(mp, s.c_str(), 0) != 0)
      {
        mpfr_clear (mp);
        throw std::invalid_argument ("mpfr_set_str");
      }
  }

  __oomph_expr( std::string const& s, mpfr_prec_t prec, int base = 0 ) : mp( new mpfr_t )
  {
    mpfr_init2(mp, prec);
    if (mpfr_set_str(mp, s.c_str(), base) != 0)
      {
        mpfr_clear (mp);
        throw std::invalid_argument ("mpfr_set_str");
      }
  }

  explicit __oomph_expr ( mpfr_srcptr f ) : mp( new mpfr_t )
  { mpfr_init2(mp, mpfr_get_prec(f)); mpfr_set(mp, f); }

  __oomph_expr ( mpfr_srcptr f, mpfr_prec_t prec ) : mp( new mpfr_t )
  { mpfr_init2(mp, prec); mpfr_set(mp, f); }

  ~__oomph_expr() { if (mp) { mpfr_clear(mp); delete[] mp; mp = nullptr; } }

  // assignment operators
  __oomph_expr & operator= ( __oomph_expr const& f )
  { mpfr_set(mp, f.mp); return *this; }

  __oomph_expr & operator= ( __oomph_expr && f )
  { mp = f.mp; f.mp = nullptr; return *this; }

  template <class T, class U>
  __oomph_expr<value_type, value_type> & operator= ( __oomph_expr<T, U> const& expr )
  { __oomph_set_expr(mp, expr); return *this; }

  template <typename T>
  __oomph_expr & operator= ( std::enable_if<std::is_integral<T>::value && std::is_signed<T>::value,T>::value n )
  { mpfr_set_si(mp, n); return *this; }

  template <typename T>
  __oomph_expr & operator= ( std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value,T>::value n )
  { mpfr_set_ui(mp, c); return *this; }

  template <typename T>
  __oomph_expr & operator= ( std::enable_if<std::is_floating_point<T>::value,T>::value f )
  { mpfr_set_d(mp, f); return *this; }

  __oomph_expr & operator= ( char const* s )
  {
    if (mpfr_set_str (mp, s, 0) != 0)
      throw std::invalid_argument ("mpfr_set_str");
    return *this;
  }

  __oomph_expr & operator= ( std::string const& s )
  {
    if (mpfr_set_str(mp, s.c_str(), 0) != 0)
      throw std::invalid_argument ("mpfr_set_str");
    return *this;
  }

  // string input/output functions
  int set_str ( char const* s, int base )
  { return mpfr_set_str(mp, s, base); }

  int set_str ( std::string const& s, int base )
  { return mpfr_set_str(mp, s.c_str(), base); }

  std::string get_str ( mp_exp_t & expo, int base = 10, size_t size = 0 ) const
  {
    __oomph_alloc_cstring temp(mpfr_get_str(0, &expo, base, size, mp));
    return std::string(temp.str);
  }

  // conversion functions
  mpfr_srcptr __get_mp() const { return mp; }

  mpfr_ptr __get_mp() { return mp; }

  mpfr_srcptr get_mpfr_t() const { return mp; }

  mpfr_ptr get_mpfr_t() { return mp; }

  signed long int get_si() const { return mpfr_get_si(mp); }

  unsigned long int get_ui() const { return mpfr_get_ui(mp); }

  double get_d() const { return mpfr_get_d(mp); }

///////////////////////////////////////////////////////////////

  // bool fits_schar_p() const { return mpf_fits_schar_p(mp); }
  // bool fits_uchar_p() const { return mpf_fits_uchar_p(mp); }
  bool fits_sint_p() const { return mpfr_fits_sint_p(mp); }
  bool fits_uint_p() const { return mpfr_fits_uint_p(mp); }
  bool fits_sshort_p() const { return mpfr_fits_sshort_p(mp); }
  bool fits_ushort_p() const { return mpfr_fits_ushort_p(mp); }
  bool fits_slong_p() const { return mpfr_fits_slong_p(mp); }
  bool fits_ulong_p() const { return mpfr_fits_ulong_p(mp); }
  // bool fits_float_p() const { return mpf_fits_float_p(mp); }
  // bool fits_double_p() const { return mpf_fits_double_p(mp); }
  // bool fits_ldouble_p() const { return mpf_fits_ldouble_p(mp); }

  // compound assignments
  __GMP_DECLARE_COMPOUND_OPERATOR(operator+=)
  __GMP_DECLARE_COMPOUND_OPERATOR(operator-=)
  __GMP_DECLARE_COMPOUND_OPERATOR(operator*=)
  __GMP_DECLARE_COMPOUND_OPERATOR(operator/=)

  __GMP_DECLARE_COMPOUND_OPERATOR_UI(operator<<=)
  __GMP_DECLARE_COMPOUND_OPERATOR_UI(operator>>=)

  __GMP_DECLARE_INCREMENT_OPERATOR(operator++)
  __GMP_DECLARE_INCREMENT_OPERATOR(operator--)

private:
  typedef mpfr_ptr value_type;
  value_type mp;
};

typedef __oomph_expr<mpfr_t, mpfr_t> mpfr_class;
*/

template <>
class __oomph_expr<mpfi_t, mpfi_t>
{
public:
  mpfr_prec_t get_prec () const { return mpfr_get_prec(mp); }

  void set_prec ( mpfr_prec_t prec ) { mpfi_set_prec(mp, prec); }

  void set_prec_raw ( mpfr_prec_t prec ) { mpfi_set_prec_raw(mp, prec); }

  // constructors and destructor
  __oomph_expr () : mp( new mpfi_t ) { mpfi_init(mp); }

  __oomph_expr ( __oomph_expr const& f ) : mp( new mpfi_t )
  { mpfi_init2(mp, f.get_prec()); mpfi_set(mp, f.mp); }

  __oomph_expr ( __oomph_expr && f ) : mp( f.mp )
  { f.mp = nullptr; }

  __oomph_expr ( __oomph_expr const& f, mpfr_prec_t prec ) : mp( new mpfi_t )
  { mpfi_init2(mp, prec); mpfi_set(mp, f.mp); }

  __oomph_expr ( __oomph_expr && f, mpfr_prec_t prec ) : mp( f.mp )
  { mpfi_set_prec(mp, prec); f.mp = nullptr; }

  template <class T, class U>
  __oomph_expr( __oomph_expr<T, U> const& expr ) : mp( new mpfi_t )
  { mpfi_init2(mp, expr.get_prec()); __oomph_set_expr(mp, expr); }

  template <class T, class U>
  __oomph_expr( __oomph_expr<T, U> const& expr, mpfr_prec_t prec ) : mp( new mpfi_t )
  { mpfi_init2(mp, prec); __oomph_set_expr(mp, expr); }

  template <typename T>
  __oomph_expr ( std::enable_if<std::is_integral<T>::value && std::is_signed<T>::value,T>::value n ) : mp( new mpfi_t )
  { mpfi_init_set_si(mp, n); }

  template <typename T>
  __oomph_expr ( std::enable_if<std::is_integral<T>::value && std::is_signed<T>::value,T>::value n, mpfr_prec_t prec ) : mp( new mpfi_t )
  { mpfi_init2(mp, prec); mpfi_set_si(mp, n); }

  template <typename T>
  __oomph_expr ( std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value,T>::value n ) : mp( new mpfi_t )
  { mpfi_init_set_ui(mp, n); }

  template <typename T>
  __oomph_expr ( std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value,T>::value n, mpfr_prec_t prec ) : mp( new mpfi_t )
  { mpfi_init2(mp, prec); mpfi_set_ui(mp, n); }

  template <typename T>
  __oomph_expr( std::enable_if<std::is_floating_point<T>::value,T>::value f ) : mp( new mpfi_t )
  { mpfi_init_set_d(mp, f); }

  template <typename T>
  __oomph_expr( std::enable_if<std::is_floating_point<T>::value,T>::value f, mpfr_prec_t prec) : mp( new mpfi_t )
  { mpfi_init2(mp, prec); mpfr_set_d(mp, f); }

  explicit __oomph_expr( char const* s ) : mp( new mpfi_t )
  {
    if (mpfi_init_set_str (mp, s, 0) != 0)
      {
        mpfi_clear (mp);
        throw std::invalid_argument ("mpfi_set_str");
      }
  }

  __oomph_expr( char const* s, mpfr_prec_t prec, int base = 0 ) : mp( new mpfi_t )
  {
    mpfi_init2(mp, prec);
    if (mpfi_set_str(mp, s, base) != 0)
      {
        mpfi_clear (mp);
        throw std::invalid_argument ("mpfi_set_str");
      }
  }

  explicit __oomph_expr( std::string const& s ) : mp( new mpfi_t )
  {
    if (mpfi_init_set_str(mp, s.c_str(), 0) != 0)
      {
        mpfi_clear (mp);
        throw std::invalid_argument ("mpfi_set_str");
      }
  }

  __oomph_expr( std::string const& s, mpfr_prec_t prec, int base = 0 ) : mp( new mpfi_t )
  {
    mpfi_init2(mp, prec);
    if (mpfi_set_str(mp, s.c_str(), base) != 0)
      {
        mpfi_clear (mp);
        throw std::invalid_argument ("mpfi_set_str");
      }
  }

  explicit __oomph_expr ( mpfi_srcptr f ) : mp( new mpfi_t )
  { mpfi_init2(mp, mpfr_get_prec(f)); mpfi_set(mp, f); }

  __oomph_expr ( mpfi_srcptr f, mpfr_prec_t prec ) : mp( new mpfi_t )
  { mpfi_init2(mp, prec); mpfi_set(mp, f); }

  ~__oomph_expr() { if (mp) { mpfi_clear(mp); delete[] mp; mp = nullptr; } }

  // assignment operators
  __oomph_expr & operator= ( __oomph_expr const& f )
  { mpfi_set(mp, f.mp); return *this; }

  __oomph_expr & operator= ( __oomph_expr && f )
  { mp = f.mp; f.mp = nullptr; return *this; }

  template <class T, class U>
  __oomph_expr<value_type, value_type> & operator= ( __oomph_expr<T, U> const& expr )
  { __oomph_set_expr(mp, expr); return *this; }

  template <typename T>
  __oomph_expr & operator= ( std::enable_if<std::is_integral<T>::value && std::is_signed<T>::value,T>::value n )
  { mpfi_set_si(mp, n); return *this; }

  template <typename T>
  __oomph_expr & operator= ( std::enable_if<std::is_integral<T>::value && std::is_unsigned<T>::value,T>::value n )
  { mpfi_set_ui(mp, c); return *this; }

  template <typename T>
  __oomph_expr & operator= ( std::enable_if<std::is_floating_point<T>::value,T>::value f )
  { mpfi_set_d(mp, f); return *this; }

  __oomph_expr & operator= ( char const* s )
  {
    if (mpfi_set_str (mp, s, 0) != 0)
      throw std::invalid_argument ("mpfi_set_str");
    return *this;
  }

  __oomph_expr & operator= ( std::string const& s )
  {
    if (mpfi_set_str(mp, s.c_str(), 0) != 0)
      throw std::invalid_argument ("mpfi_set_str");
    return *this;
  }

  // string input/output functions
  int set_str ( char const* s, int base )
  { return mpfi_set_str(mp, s, base); }

  int set_str ( std::string const& s, int base )
  { return mpfi_set_str(mp, s.c_str(), base); }

  std::string get_str ( mp_exp_t & expo, int base = 10, size_t size = 0 ) const
  {
    __oomph_alloc_cstring temp(mpfi_get_str(0, &expo, base, size, mp));
    return std::string(temp.str);
  }

  // conversion functions
  mpfi_srcptr __get_mp() const { return mp; }

  mpfi_ptr __get_mp() { return mp; }

  mpfi_srcptr get_mpfr_t() const { return mp; }

  mpfi_ptr get_mpfr_t() { return mp; }

  signed long int get_si() const { return mpfi_get_si(mp); }

  unsigned long int get_ui() const { return mpfi_get_ui(mp); }

  double get_d() const { return mpfi_get_d(mp); }

///////////////////////////////////////////////////////////////

  // bool fits_schar_p() const { return mpf_fits_schar_p(mp); }
  // bool fits_uchar_p() const { return mpf_fits_uchar_p(mp); }
  bool fits_sint_p() const { return mpfi_fits_sint_p(mp); }
  bool fits_uint_p() const { return mpfi_fits_uint_p(mp); }
  bool fits_sshort_p() const { return mpfi_fits_sshort_p(mp); }
  bool fits_ushort_p() const { return mpfi_fits_ushort_p(mp); }
  bool fits_slong_p() const { return mpfi_fits_slong_p(mp); }
  bool fits_ulong_p() const { return mpfi_fits_ulong_p(mp); }
  // bool fits_float_p() const { return mpf_fits_float_p(mp); }
  // bool fits_double_p() const { return mpf_fits_double_p(mp); }
  // bool fits_ldouble_p() const { return mpf_fits_ldouble_p(mp); }

  // compound assignments
  __GMP_DECLARE_COMPOUND_OPERATOR(operator+=)
  __GMP_DECLARE_COMPOUND_OPERATOR(operator-=)
  __GMP_DECLARE_COMPOUND_OPERATOR(operator*=)
  __GMP_DECLARE_COMPOUND_OPERATOR(operator/=)

  __GMP_DECLARE_COMPOUND_OPERATOR_UI(operator<<=)
  __GMP_DECLARE_COMPOUND_OPERATOR_UI(operator>>=)

  __GMP_DECLARE_INCREMENT_OPERATOR(operator++)
  __GMP_DECLARE_INCREMENT_OPERATOR(operator--)

private:
  typedef mpfi_ptr value_type;
  value_type mp;
};

typedef __oomph_expr<mpfi_t, mpfi_t> mpfi_class;
