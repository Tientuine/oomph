#include "interval.hpp"

//using namespace mpoofi;

mpfi_class negG ( mpfi_class const& phi )
{
  return phi * cot( phi / 2ul );
}

auto
H ( mpfi_class const& a, mpfi_class const& b ) -> mpfi_class
{
	static mpfi_class ROOT3 { sqrt(mpfi_class{3UL,200}) };
	return (((a + ROOT3 * b) * sinh(a)) - ((ROOT3 * a - b) * sin(b))) / (cosh(a) - cos(b));
}

auto
F ( mpfi_class const& k, mpfi_class const& R ) -> mpfi_class
{
	mpfi_class k2 { sqr(k) }; // compute and store k^2
	mpfi_class k2R13 { cbrt(k2 * R) };
	mpfi_class k2_k2R13 { k2 + k2R13 }; //!!!

	mpfi_class tmp1 { sqrt(k2 * k2_k2R13 + sqr(k2R13)) * 2UL };  // 2*sqrt(  k^4 + (k^2 * (k^2 * R)^(1/3)) + (k^2 * R)^(2/3) )
	mpfi_class tmp2 { k2 + k2_k2R13 }; // 2*k^2 + (k^2 * R)^(1/3)
	mpfi_class a { sqrt(tmp1 + tmp2) };
	mpfi_class b { sqrt(tmp1 - tmp2) };

	// compute phi(k,R)
	mpfi_class phi { sqrt(k2R13 - k2) * 2UL };

	return  H ( a, b ) - negG( phi );
}

auto
Rprimek ( mpfi_class const& k, mpfi_class const& R ) -> mpfi_class
{
	mpfi_class root3 {3,200};
	root3 = sqrt(root3);

	mpfi_class k2 {sqr(k)};
	mpfi_class k22 {2*k2};
	mpfi_class k3 {k*k2};
	mpfi_class k4 {sqr(k2)};
	mpfi_class k2R {R*k2};
	mpfi_class k2R13 {cbrt(k2R)};
	mpfi_class k2R23 {sqr(k2R13)};
	mpfi_class k2_k2R13 {k2*k2R13};
	mpfi_class SQRT1 {sqrt(k2R13 - k2)};
	mpfi_class SQRT2 {sqrt(k4 + k2_k2R13 + k2R23)};
	mpfi_class SQRT3 {sqrt(2*SQRT2 + k22 + k2R13)};
	mpfi_class SQRT4 {sqrt(2*SQRT2 - k22 - k2R13)};
	mpfi_class k2R13x3 {3*k2R13};
	mpfi_class k2R23x3 {3*k2R23};
	mpfi_class ADD2 {SQRT3+root3*SQRT4};
	mpfi_class SUB2 {root3*SQRT3-SQRT4};
	mpfi_class kx2 {2*k};
	mpfi_class kx4 {4*k};
	mpfi_class k3x4 {4*k3}; // 4*k^3
	mpfi_class kRx4 {kx4*R}; // 4*R*k
	mpfi_class kRx2 {kx2*R}; // 2*R*k
	mpfi_class k3Rx2 {2*R*k3};
	mpfi_class k2R13x2k {2*k*k2R13};
	mpfi_class TERM0 {(kx2 - kRx2/k2R23x3)};
	mpfi_class TERM1 {(kx4 + (k2R13x2k + k3x4 + k3Rx2/k2R23x3 + kRx4/k2R13x3)/SQRT2 + kRx2/k2R23x3)};
	mpfi_class TERM2 {(kx4 - (k2R13x2k + k3x4 + k3Rx2/k2R23x3 + kRx4/k2R13x3)/SQRT2 + kRx2/k2R23x3)};

	return  cot(SQRT1)*TERM0/SQRT1 - (1+sqr(cot(SQRT1)))*TERM0 - ( (TERM1/(2*SQRT3)-root3*TERM2/(2*SQRT4))*sinh(SQRT3) + ADD2*cosh(SQRT3)*TERM1/(2*SQRT3) - (root3*TERM1/(2*SQRT3)+TERM2/(2*SQRT4))*sin(SQRT4) + SUB2*cos(SQRT4)*TERM2/(2*SQRT4) ) / (cos(SQRT4) - cosh(SQRT3)) - ( (SUB2*sin(SQRT4) - ADD2*sinh(SQRT3)) * ( sin(SQRT4)*TERM2/(2*SQRT4) - sinh(SQRT3)*TERM1/(2*SQRT3) ) ) / sqr(cos(SQRT4) - cosh(SQRT3));
}

int main(int argc, char** argv)
{
  mpfi_class k {"[ 2.682, 2.683 ]", 200};
  mpfi_class R {"[ 1.1006e3, 1.1007e3 ]", 200};

  for (int i = 1; i < 100000; ++i)
  {
    F ( k, R ); // expect [-0.06983447946530,0.06980358175310]
    Rprimek ( k, R ); // expect [?,?]
  }

  std::cout.precision(32);
  std::cout << "k: " << k << std::endl;
  std::cout << "R: " << R << std::endl;
  std::cout << "F: " << F ( k, R ) << std::endl;
  std::cout << "dF/dk: " << Rprimek ( k, R ) << std::endl;

  return 0;
}
