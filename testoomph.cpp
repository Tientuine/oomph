#include "interval.hpp"
#include <iostream>

int main()
{
  mpfi_class x("[2.1,2.3]", 200);
  mpfi_class y("[-1.0,0.4]", 200);
  mpfi_class z(0);

  for (auto i = 0; i < 10000000; ++i) {
    z = x + y * (x - y);
  }

  std::cout << z.get_str() << std::endl;
  return 0;
}

